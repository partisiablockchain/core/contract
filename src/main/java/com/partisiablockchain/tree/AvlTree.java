package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Hash.HashAndSize;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation of a immutable self-balancing <a href="https://en.wikipedia.org/wiki/AVL_tree">AVL
 * Tree</a>.
 *
 * <p>Primary use case is for mappings in contract states, where the {@link AvlTree} can be
 * partially loaded from the underlying state storage; allowing for very large contract states. In
 * that kind of configuration, it is possible to use a <a
 * href="https://en.wikipedia.org/wiki/Merkle_tree">Merkle Tree construction</a> to efficiently
 * verify the tree contents.
 *
 * <h2>Implementation</h2>
 *
 * <p>Any given {@link AvlTree} is an instance of:
 *
 * <ul>
 *   <li>{@link EmptyTree}: Empty {@link AvlTree}.
 *   <li>{@link AvlLeaf}: Singleton {@link AvlTree}.
 *   <li>{@link AvlComposite}: {@link AvlTree} of two or more elements.
 * </ul>
 *
 * <h2>Invariants</h2>
 *
 * <ul>
 *   <li>Cannot contain {@code null} keys.
 *   <li>Can contain {@code null} values.
 *   <li>Tree is {@link EmptyTree} if and only if {@link AvlTree#size} is 0.
 *   <li>Tree is {@link AvlLeaf} if and only if {@link AvlTree#size} is 1.
 *   <li>Tree is {@link AvlComposite} if and only if {@link AvlTree#size} is 2 or greater.
 *   <li>Trees with size 1 or greater cannot contain {@link EmptyTree}, but can contain both {@link
 *       AvlLeaf} and {@link AvlComposite}.
 * </ul>
 *
 * @param <K> the type of keys
 * @param <V> the type of values to associate with keys
 * @see <a href="https://en.wikipedia.org/wiki/AVL_tree">AVL Tree, Wikipedia</a>
 * @see <a href="https://en.wikipedia.org/wiki/Merkle_tree">Merkle Tree, Wikipedia</a>
 */
@Immutable
@CheckReturnValue
public abstract class AvlTree<
    @ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V> {

  /**
   * Construct a new empty AvlTree.
   *
   * @param <K> key type
   * @param <V> value type
   * @return new empty tree
   */
  public static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> create() {
    return new EmptyTree<>();
  }

  /**
   * Construct a new AvlTree populated with the values from the supplied map.
   *
   * @param <K> key type
   * @param <V> value type
   * @param initial the initial key value pairs for the tree
   * @return new populated tree
   */
  public static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> create(Map<K, V> initial) {
    List<Map.Entry<K, V>> sortedEntries =
        initial.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toList());
    AvlTree<K, V> tree = new EmptyTree<>();
    for (Map.Entry<K, V> entry : sortedEntries) {
      tree = tree.set(entry.getKey(), entry.getValue());
    }
    return tree;
  }

  /**
   * Associates the specified value with the specified key.
   *
   * <p>This operation is idempotent: Set the same key-value pair twice in a row produces the same
   * result as setting it once.
   *
   * @param key key with which the specified value is to be associated. Not {@code null}able.
   * @param value value to be associated with the specified key
   * @return new tree with key and associated value
   */
  public abstract AvlTree<K, V> set(K key, V value);

  /**
   * Removes the mapping for a key from this tree if it is present.
   *
   * <p>This operation is idempotent: Removing the same key twice in a row produces the same result
   * as removing it once.
   *
   * @param key key whose mapping is to be removed from the tree. Not {@code null}able.
   * @return new tree without key and associated value
   */
  public abstract AvlTree<K, V> remove(K key);

  /**
   * The number of key value pairs in the tree.
   *
   * @return the number of key value pairs
   */
  public abstract int size();

  /**
   * Retrieve the value associated with the given key if any.
   *
   * @param key key whose mapping is to be return. Not {@code null}able.
   * @return the associated value or {@code null} if none found
   */
  public abstract V getValue(K key);

  abstract K maxKeyInTree();

  abstract K minKeyInTree();

  abstract byte height();

  abstract int balance();

  abstract K getKey();

  /**
   * Write a tree.
   *
   * @param writer writes a tree
   */
  public abstract void write(TreeWriter<K, V> writer);

  /**
   * Write a tree to a fresh storage.
   *
   * @param writer writes a tree
   */
  public abstract void freshWrite(TreeWriter<K, V> writer);

  /**
   * Calculate hash of root node for a tree.
   *
   * @param treeWriter writes an avl tree
   * @return hash of root node
   */
  public abstract HashAndSize calculateHash(TreeWriter<K, V> treeWriter);

  abstract void keySet(Set<K> keys);

  /**
   * Get a set of all keys in this tree.
   *
   * @return a set containing all keys of this tree. Never {@code null}, and cannot contain {@code
   *     null}.
   */
  public Set<K> keySet() {
    Set<K> keys = new LinkedHashSet<>();
    keySet(keys);
    return keys;
  }

  /**
   * Get a list of all values in this tree.
   *
   * @return a list containing all values contained in this tree. Can contain {@code null}.
   */
  public List<V> values() {
    return keySet().stream().map(this::getValue).collect(Collectors.toList());
  }

  /**
   * Checks if tree contains given key.
   *
   * @param key given key. Not {@code null}able.
   * @return true if tree contains key
   */
  public boolean containsKey(K key) {
    return getValue(key) != null;
  }

  /**
   * Visit a path in the tree.
   *
   * @param writer can write an avl tree or sub-tree. Not {@code null}able.
   * @param key next node to visit. Not {@code null}able.
   * @param pathVisitor visitor to visit nodes with. Not {@code null}able.
   */
  public abstract void visitPath(TreeWriter<K, V> writer, K key, TreePathVisitor<K, V> pathVisitor);

  /**
   * Get (key, value) for the n items following the provided key (excluding the key itself). To get
   * the first n entries use the {@code null} key.
   *
   * @param key The initial key. {@code null} is allowed. If {@code null}, the method returns the
   *     first n entries.
   * @param n the number of items to get.
   * @param numToSkip number of elements to skip before collecting entries
   * @return list of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  abstract AvlGetNextResult<K, V> getNextInternal(K key, int n, int numToSkip);

  /**
   * Get (key, value) for the n items following the provided key (excluding the key itself). To get
   * the first n entries use the {@code null} key.
   *
   * @param key The initial key. {@code null} is allowed. If {@code null}, the method returns the
   *     first n entries.
   * @param n the number of items to get.
   * @param skip number of elements to skip before collecting entries
   * @return list of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  public List<Map.Entry<K, V>> getNextN(K key, int n, int skip) {
    return getNextInternal(key, n, skip).entries();
  }

  /**
   * Get (key, value) for the n items following the provided key (excluding the key itself). To get
   * the first n entries use the {@code null} key.
   *
   * @param key The initial key. {@code null} is allowed. If {@code null}, the method returns the
   *     first n entries.
   * @param n the number of items to get.
   * @return list of n key, value pairs. May return fewer than n values if fewer matching items
   *     exist in the tree.
   */
  public List<Map.Entry<K, V>> getNextN(K key, int n) {
    return getNextN(key, n, 0);
  }

  abstract Hash hash();

  /**
   * Get the modified keys between this and other avl tree. Only gets keys following the base key
   * (exclusive) and gets at most n keys. This method should only be called on avl trees that have
   * just been read from or written to disc, as it makes use of their hashes to compare equal trees.
   *
   * @param <K> key type
   * @param <V> value type
   * @param tree1 the first avl tree to compare.
   * @param tree2 the second avl tree to compare.
   * @param key The initial key. {@code null} is allowed. If {@code null}, the method starts from
   *     the beginning.
   * @param n the number of keys to get.
   * @return list of at most n modified keys. May return fewer than n keys if fewer differing keys
   *     exist.
   */
  public static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      List<K> modifiedKeys(AvlTree<K, V> tree1, AvlTree<K, V> tree2, K key, int n) {
    TreeWalker<K, V> walker1 = new TreeWalker<>(tree1);
    TreeWalker<K, V> walker2 = new TreeWalker<>(tree2);
    List<K> nodes = new ArrayList<>();
    K baseKey = key;
    AvlTree<K, V> node1 = walker1.currentNode();
    AvlTree<K, V> node2 = walker2.currentNode();
    // Invariant: If a leaf key of a tree walker is smaller than the minimum key of the other tree
    // walker, then the leaf key is in the set of modified keys
    while (node1 != null && node2 != null && nodes.size() < n) {
      if (isSameTree(node1, node2)) {
        node1 = walker1.skipChildren();
        node2 = walker2.skipChildren();
        continue;
      }
      // Check to skip for base key. When the walkers both walk to a point where they are past the
      // base key, it is set to null as an optimization
      if (baseKey != null && node1.maxKeyInTree().compareTo(baseKey) <= 0) {
        node1 = walker1.skipChildren();
        continue;
      }
      if (baseKey != null && node2.maxKeyInTree().compareTo(baseKey) <= 0) {
        node2 = walker2.skipChildren();
        continue;
      }

      if (node1 instanceof AvlComposite<K, V> && node2 instanceof AvlComposite<K, V>) {
        // Both composite. Walk until one becomes a leaf.

        node1 = walker1.nextNode();
        node2 = walker2.nextNode();
      } else if (node1 instanceof AvlLeaf<K, V> leaf1
          && node2 instanceof AvlComposite<K, V> comp2) {
        // A leaf and a composite. If the leaf is smaller than the composite, add the leaf key to
        // modified keys and walk it. Else walk the composite.

        if (isSmaller(leaf1, comp2)) {
          nodes.add(leaf1.getKey());
          node1 = walker1.nextNode();
          baseKey = null;
        } else {
          node2 = walker2.nextNode();
        }
      } else if (node1 instanceof AvlComposite<K, V> comp1) {
        AvlLeaf<K, V> leaf2 = (AvlLeaf<K, V>) node2;
        // A leaf and a composite. Symmetrical to previous case

        if (isSmaller(leaf2, comp1)) {
          nodes.add(leaf2.getKey());
          node2 = walker2.nextNode();
          baseKey = null;
        } else {
          node1 = walker1.nextNode();
        }
      } else {
        AvlLeaf<K, V> leaf1 = (AvlLeaf<K, V>) node1;
        AvlLeaf<K, V> leaf2 = (AvlLeaf<K, V>) node2;
        // Both leafs. They are guaranteed to be different as otherwise they would be caught in the
        // same tree check. Add the smallest key to modified and walk it. If same key, add it and
        // walk both.

        baseKey = null;
        int compare = leaf1.getKey().compareTo(leaf2.getKey());
        if (compare < 0) {
          nodes.add(leaf1.getKey());
          node1 = walker1.nextNode();
        } else if (compare > 0) {
          nodes.add(leaf2.getKey());
          node2 = walker2.nextNode();
        } else {
          nodes.add(leaf1.getKey());
          node1 = walker1.nextNode();
          node2 = walker2.nextNode();
        }
      }
    }

    // Invariant when getting here:
    // Either nodes.size() = n or walker1.currentNode == null or walker2.currentNode == null
    // It follows that at most one of the two following calls can add new nodes
    getRemaining(walker1, nodes, baseKey, n);
    getRemaining(walker2, nodes, baseKey, n);
    return nodes;
  }

  /** Return whether the trees are the same. */
  private static <K extends Comparable<K>, V> boolean isSameTree(
      AvlTree<K, V> node1, AvlTree<K, V> node2) {
    return node1.hash().equals(node2.hash());
  }

  /** Is the tree smaller as defined by the minimum key of each tree. */
  private static <K extends Comparable<K>, V> boolean isSmaller(
      AvlTree<K, V> node1, AvlTree<K, V> node2) {
    return node1.minKeyInTree().compareTo(node2.minKeyInTree()) < 0;
  }

  /** Add the remaining keys to nodes, constrained to the base key and max number n. */
  private static <K extends Comparable<K>, V> void getRemaining(
      TreeWalker<K, V> walker, List<K> nodes, K key, int n) {
    AvlTree<K, V> node = walker.currentNode();
    while (node != null && nodes.size() < n) {
      if (key != null && node.maxKeyInTree().compareTo(key) <= 0) {
        node = walker.skipChildren();
      } else {
        if (node instanceof AvlLeaf<K, V> leaf) {
          nodes.add(leaf.getKey());
        }
        node = walker.nextNode();
      }
    }
  }
}
