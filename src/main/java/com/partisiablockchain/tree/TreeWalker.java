package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayDeque;

/** A pre-order walk through an AvlTree. */
public final class TreeWalker<K extends Comparable<K>, V> {
  private AvlTree<K, V> currentNode;

  private final ArrayDeque<AvlComposite<K, V>> stack;

  /**
   * Constructor.
   *
   * @param root root of the avl tree.
   */
  public TreeWalker(AvlTree<K, V> root) {
    if (root instanceof EmptyTree<K, V>) {
      this.currentNode = null;
    } else {
      this.currentNode = root;
    }
    this.stack = new ArrayDeque<>();
  }

  /**
   * Advance to the next node in the walk.
   *
   * @return the next node, or null if the end is reached
   */
  public AvlTree<K, V> nextNode() {
    if (currentNode instanceof AvlComposite<K, V> composite) {
      stack.push(composite);
      currentNode = composite.getLeft();
      return currentNode;
    } else if (currentNode instanceof AvlLeaf<K, V>) {
      return popStack();
    } else { // EmptyTree
      currentNode = null;
      return null;
    }
  }

  private AvlTree<K, V> popStack() {
    AvlTree<K, V> current = currentNode;
    if (stack.isEmpty()) {
      currentNode = null;
      return null;
    }
    while (current == stack.peek().getRight()) {
      current = stack.pop();
      if (stack.isEmpty()) {
        currentNode = null;
        return null;
      }
    }
    currentNode = stack.peek().getRight();
    return currentNode;
  }

  /**
   * Skip children of this node and advance to next node.
   *
   * @return the next node after skipping children
   */
  public AvlTree<K, V> skipChildren() {
    return popStack();
  }

  /**
   * Get current node without advancing the walk.
   *
   * @return the current walk
   */
  public AvlTree<K, V> currentNode() {
    return currentNode;
  }
}
