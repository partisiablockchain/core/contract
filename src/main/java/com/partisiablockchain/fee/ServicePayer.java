package com.partisiablockchain.fee;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.immutable.FixedList;

/** Pays fees for services on the blockchain. */
public interface ServicePayer {

  /**
   * Pay the target node for the completed computation.
   *
   * @param gas the amount of gas to pay the participating node
   * @param target the node to pay
   */
  void payServiceFees(long gas, BlockchainAddress target);

  /**
   * Pay the target nodes for the completed computation. The gas will be distributed among the nodes
   * according to the supplied weights.
   *
   * @param gas the amount of gas used for the completed computation
   * @param nodes the participating nodes to pay
   * @param weights the weights defining how the gas should be divided among the nodes
   * @throws IllegalArgumentException if nodes.size() != weights.size()
   */
  default void payServiceFees(
      long gas, FixedList<BlockchainAddress> nodes, FixedList<Integer> weights) {
    FeeDistributor.distribute(gas, nodes, weights, this::payServiceFees);
  }
}
