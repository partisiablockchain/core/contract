package com.partisiablockchain.contract.reflect;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** This annotation allows customizing the generated RPC code. */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.PARAMETER, ElementType.RECORD_COMPONENT})
public @interface RpcType {

  /**
   * Whether the parameter is nullable or not. Nullable values are preceded by a boolean indicating
   * if the value is null or not.
   *
   * @return true if nullable
   */
  boolean nullable() default false;

  /**
   * Whether the annotated parameter/field is a signed integer type.
   *
   * @return the signed flag
   */
  boolean signed() default true;

  /**
   * The byte size of the parameter. Used to mark sized byte arrays and unsigned integers.
   *
   * @return the size of the parameter
   */
  int size() default -1;
}
