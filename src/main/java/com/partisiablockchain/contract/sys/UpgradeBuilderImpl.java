package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.ContractEvent;
import com.secata.stream.DataStreamSerializable;
import java.util.Objects;
import java.util.function.Consumer;

/** Default implementation of {@link UpgradeBuilder} used to create a contract upgrade. */
public final class UpgradeBuilderImpl implements UpgradeBuilder {

  private final Consumer<ContractEvent> eventGroup;
  private final Long cost;
  private final Upgrade upgrade;

  private UpgradeBuilderImpl(Consumer<ContractEvent> eventGroup, Long cost, Upgrade upgrade) {
    this.eventGroup = eventGroup;
    this.cost = cost;
    this.upgrade = upgrade;
  }

  /**
   * Creates a new UpgradeBuilderImpl.
   *
   * @param contractEvents the event group that stores the event
   * @param contract the contract address
   */
  public UpgradeBuilderImpl(Consumer<ContractEvent> contractEvents, BlockchainAddress contract) {
    this(
        contractEvents,
        null,
        new Upgrade(
            Objects.requireNonNull(contract, "Cannot upgrade a null contract"),
            null,
            null,
            null,
            null));
  }

  @Override
  public UpgradeBuilder withNewBinderJar(byte[] newBinderJar) {
    return new UpgradeBuilderImpl(
        eventGroup,
        cost,
        new Upgrade(
            upgrade.contract, newBinderJar, upgrade.newContractJar, upgrade.newAbi, upgrade.rpc));
  }

  @Override
  public UpgradeBuilder withNewContractJar(byte[] newContractJar) {
    return new UpgradeBuilderImpl(
        eventGroup,
        cost,
        new Upgrade(
            upgrade.contract, upgrade.newBinderJar, newContractJar, upgrade.newAbi, upgrade.rpc));
  }

  @Override
  public UpgradeBuilder withNewAbi(byte[] newAbi) {
    return new UpgradeBuilderImpl(
        eventGroup,
        cost,
        new Upgrade(
            upgrade.contract, upgrade.newBinderJar, upgrade.newContractJar, newAbi, upgrade.rpc));
  }

  @Override
  public UpgradeBuilder withUpgradeRpc(DataStreamSerializable rpc) {
    return new UpgradeBuilderImpl(
        eventGroup,
        cost,
        new Upgrade(
            upgrade.contract, upgrade.newBinderJar, upgrade.newContractJar, upgrade.newAbi, rpc));
  }

  @Override
  public UpgradeBuilder allocateCost(long cost) {
    return new UpgradeBuilderImpl(
        eventGroup,
        cost,
        new Upgrade(
            upgrade.contract,
            upgrade.newBinderJar,
            upgrade.newContractJar,
            upgrade.newAbi,
            upgrade.rpc));
  }

  @Override
  public UpgradeBuilder allocateRemainingCost() {
    return new UpgradeBuilderImpl(
        eventGroup,
        null,
        new Upgrade(
            upgrade.contract,
            upgrade.newBinderJar,
            upgrade.newContractJar,
            upgrade.newAbi,
            upgrade.rpc));
  }

  @Override
  public void sendFromContract() {
    Objects.requireNonNull(upgrade.newBinderJar);
    Objects.requireNonNull(upgrade.newContractJar);
    eventGroup.accept(new ContractEventUpgrade(upgrade, false, cost));
  }

  @Override
  public void send() {
    Objects.requireNonNull(upgrade.newBinderJar);
    Objects.requireNonNull(upgrade.newContractJar);
    eventGroup.accept(new ContractEventUpgrade(upgrade, true, cost));
  }
}
