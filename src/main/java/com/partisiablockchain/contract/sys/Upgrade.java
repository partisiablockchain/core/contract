package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;

/** Struct representing an event for upgrading a contract. */
public final class Upgrade {

  /** Address of contract to upgrade. */
  public final BlockchainAddress contract;

  /** New binder jar. */
  public final byte[] newBinderJar;

  /** New contract jar. */
  public final byte[] newContractJar;

  /** New abi. */
  public final byte[] newAbi;

  /** Invocation data for contract upgrade. */
  public final DataStreamSerializable rpc;

  Upgrade(
      BlockchainAddress contract,
      byte[] newBinderJar,
      byte[] newContractJar,
      byte[] newAbi,
      DataStreamSerializable rpc) {
    this.contract = contract;
    this.newBinderJar = newBinderJar;
    this.newContractJar = newContractJar;
    this.newAbi = newAbi;
    this.rpc = rpc;
  }
}
