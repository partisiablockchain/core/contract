package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.ContractEvent;

/** A single complete upgrade event launched from the event manager. */
public final class ContractEventUpgrade implements ContractEvent {

  /** Upgrade interaction. */
  public final Upgrade upgrade;

  /** True if the sender is the same as the transaction. */
  public final boolean originalSender;

  /** The allocated cost. */
  public Long allocatedCost;

  /**
   * Creates a new contract upgrade event.
   *
   * @param upgrade upgrade interaction
   * @param originalSender true if send as the original transaction sender
   * @param allocatedCost the allocated cost, can be null
   */
  public ContractEventUpgrade(Upgrade upgrade, boolean originalSender, Long allocatedCost) {
    this.upgrade = upgrade;
    this.originalSender = originalSender;
    this.allocatedCost = allocatedCost;
  }
}
