package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;

/** Builder used to create a contract upgrade event. */
public interface UpgradeBuilder {

  /**
   * Sets the new binder jar for the upgrade.
   *
   * @param newBinderJar the new binder jar as bytes
   * @return the builder
   */
  UpgradeBuilder withNewBinderJar(byte[] newBinderJar);

  /**
   * Sets the new contract jar for the upgrade.
   *
   * @param newContractJar the new contract jar as bytes
   * @return the builder
   */
  UpgradeBuilder withNewContractJar(byte[] newContractJar);

  /**
   * Sets the new abi for the upgrade.
   *
   * @param newAbi the new abi as bytes
   * @return the builder
   */
  UpgradeBuilder withNewAbi(byte[] newAbi);

  /**
   * Sets the upgrade rpc for the event.
   *
   * @param upgradeRpc the upgrade rpc
   * @return the builder
   */
  UpgradeBuilder withUpgradeRpc(DataStreamSerializable upgradeRpc);

  /**
   * Sets the cost that is allocated for the event.
   *
   * @param cost the cost of the event
   * @return the builder
   */
  UpgradeBuilder allocateCost(long cost);

  /**
   * Sets the cost that is allocated for the event to be the remaining gas.
   *
   * @return the builder
   */
  UpgradeBuilder allocateRemainingCost();

  /** Sends the build interaction as the current contract. */
  void sendFromContract();

  /** Sends the upgrade interaction. */
  void send();
}
