package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This state is the accessors and mutators to the state in the {@link ZkContract}.
 *
 * <p>The state is divided into three parts:
 *
 * <ul>
 *   <li>The global state - similar to the state in {@link
 *       com.partisiablockchain.contract.pub.PubContract}
 *   <li>The user specific state - each user can have a state (e.g. block time to enter the smart
 *       contract)
 *   <li>the variable specific state - if a variable has some public part (a secret bid and a public
 *       commitment of this bid)
 * </ul>
 *
 * @param <ZkOpenT> the state related to each variable
 */
public interface ZkState<ZkOpenT extends StateSerializable, ZkClosedT extends ZkClosed<ZkOpenT>> {

  /**
   * Starts the computation for this zero knowledge computation, updates calculation status and
   * calculate for. NOTE: The list of information has the same size as the number of created output
   * variables; if you need three variables as a result, then make sure you add three variables
   * here.
   *
   * @param information the information that will be attached to the output variables
   */
  void startComputation(List<ZkOpenT> information);

  /**
   * Gets the current calculation status for this zero knowledge computation.
   *
   * @return current calculation status
   */
  CalculationStatus getCalculationStatus();

  /**
   * Gets a pending input with a specific id in the smart contract.
   *
   * @param id the id to lookup
   * @return the pending input
   */
  ZkClosedT getPendingInput(int id);

  /**
   * Gets every pending (and hence unconfirmed) closed variable. These variables must be confirmed
   * to be used in the computation, this requires interaction by the nodes.
   *
   * @return the list of variables
   */
  List<ZkClosedT> getPendingInputs();

  /**
   * Gets every pending (and hence unconfirmed) closed variable for a specific account. These
   * variables must be confirmed to be used in the computation, this requires interaction by the
   * nodes.
   *
   * @param account the account
   * @return the list of variables
   */
  default List<ZkClosedT> getPendingInputs(BlockchainAddress account) {
    return getPendingInputs().stream()
        .filter((variable) -> account.equals(variable.getOwner()))
        .collect(Collectors.toUnmodifiableList());
  }

  /**
   * Deletes a pending input owned by the current user.
   *
   * @param id the id to delete.
   */
  void deletePendingInput(int id);

  /**
   * Gets a variable with a specific id in the smart contract.
   *
   * @param id the id to lookup
   * @return the variable
   */
  ZkClosedT getVariable(int id);

  /**
   * Gets every variable in the smart contract.
   *
   * @return the list of variables
   */
  Collection<ZkClosedT> getVariables();

  /**
   * Gets every confirmed closed variable for a specific account. Each variable will be included as
   * a variable in the computation.
   *
   * @param owner the user to look up variables for
   * @return the list of variables
   */
  default Collection<ZkClosedT> getVariables(BlockchainAddress owner) {
    return getVariables().stream()
        .filter(zkClosedT -> zkClosedT.getOwner().equals(owner))
        .collect(Collectors.toList());
  }

  /**
   * Transfers ownership of a specific variable.
   *
   * @param id the variable id
   * @param newOwner the new owner for the variable.
   */
  void transferVariable(int id, BlockchainAddress newOwner);

  /**
   * Deletes a variable owned by the current user.
   *
   * @param id the id to delete.
   */
  void deleteVariable(int id);

  /**
   * Opens variables on chain, this allows conditional reveals of any output variable.
   *
   * @param ids the variable ids to output.
   */
  void openVariables(List<Integer> ids);

  /**
   * Ask the computation nodes to add an attestation to a piece of data.
   *
   * @param data the data to attest
   */
  void attestData(byte[] data);

  /**
   * Get the attestation using the ID supplied to the {@link ZkContract#onAttestationComplete}.
   *
   * @param id an attestation identifier
   * @return an attestation.
   */
  DataAttestation getAttestation(int id);

  /**
   * Get the list of attestation request identifiers.
   *
   * @return the list of attestation IDs.
   */
  List<Integer> getAttestationIds();

  /**
   * Completes the output phase and cleans up the variable set. This transition is the only point
   * where the contract can delete variables that are no longer in use but owned by others/users.
   *
   * @param deleteVariableIds the list of variables that should be deleted.
   */
  void outputComplete(int... deleteVariableIds);

  /** Signals that this contract has ended and have no further interactions. */
  void contractDone();
}
