package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Callback interface for whenever input is added, remember to call {@link #expectBitLength}.
 *
 * @param <ZkOpenT> the type of the variable information for this input variable
 */
public interface SecretInputBuilder<ZkOpenT> {

  /** Seals the variable on the computation nodes, making it hidden for the owner. */
  void sealVariable();

  /**
   * Sets the open part of this variable.
   *
   * @param information the information of this variable
   */
  void set(ZkOpenT information);

  /**
   * Must be called to let the nodes know how long the input is going to be.
   *
   * @param bitLength the number of bit (secret shared) in the input
   */
  void expectBitLength(int bitLength);

  /**
   * Secret input can either be given via a synchronous channel (onChain) or asynchronous
   * (offChain). This method exposes the input method type and is true if the method can be expected
   * to terminate (synchronous).
   *
   * @return true if the input method is synchronous
   */
  boolean isSynchronous();
}
