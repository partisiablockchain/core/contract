package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;

/** Issues smart contract upgrade event. */
public final class BinderUpgrade implements BinderEvent {

  /** Target contract address. */
  public final BlockchainAddress contract;

  /** New binder jar. */
  public final byte[] newBinderJar;

  /** New contract jar. */
  public final byte[] newContractJar;

  /** New abi. */
  public final byte[] newAbi;

  /** Invocation data for contract upgrade. */
  public final byte[] rpc;

  /** Sends as the original sender. */
  public final boolean originalSender;

  /** Allocated cost. */
  public final long allocatedCost;

  /**
   * Creates a new binder event to upgrade contract.
   *
   * @param contract address of contract to upgrade
   * @param newBinderJar new binder jar
   * @param newContractJar new contract jar
   * @param newAbi new abi
   * @param rpc contract upgrade rpc
   * @param originalSender send as the original sender
   * @param allocatedCost cost allocated to this event
   */
  public BinderUpgrade(
      BlockchainAddress contract,
      byte[] newBinderJar,
      byte[] newContractJar,
      byte[] newAbi,
      byte[] rpc,
      boolean originalSender,
      long allocatedCost) {
    this.contract = contract;
    this.newBinderJar = newBinderJar;
    this.newContractJar = newContractJar;
    this.newAbi = newAbi;
    this.rpc = rpc;
    this.originalSender = originalSender;
    this.allocatedCost = allocatedCost;
  }
}
