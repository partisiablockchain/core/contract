package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * A result of a call to a binder.
 *
 * @param <StateT> the contract state type.
 * @param <EventT> the type of events that binder creates.
 */
public interface BinderResult<StateT, EventT extends BinderEvent> {

  /**
   * The updated state of contract after interaction.
   *
   * @return the new state
   */
  StateT getState();

  /**
   * Gets the invocations from this result.
   *
   * @return the list of events
   */
  List<EventT> getInvocations();

  /**
   * Gets the call result this binder result - this can either be a byte array or new callback
   * stack.
   *
   * @return the list of event groups
   */
  CallResult getCallResult();
}
