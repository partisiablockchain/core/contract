package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.binder.pub.PubBinderContract;
import com.partisiablockchain.binder.sys.SysBinderContract;
import com.partisiablockchain.contract.SharedContext;

/**
 * Base context for binder contracts, including methods for CPU gas accounting.
 *
 * @see PubBinderContract
 * @see SysBinderContract
 */
@CheckReturnValue
public interface BinderContext extends SharedContext {

  /**
   * Initial amount of gas available for CPU and event creation in this event.
   *
   * <p>This number number only reflects the initial amount, and is not influenced by {@link
   * #registerCpuFee}. There is no way to determine how much gas has been consumed by {@link
   * #registerCpuFee}; binders must maintain this information themselves.
   *
   * @return Initial amount of gas available for this event.
   */
  long availableGas();

  /**
   * Register additional CPU usage for the current event.
   *
   * <p>This method is additive and not idempotent. Calling it twice with the same number is equal
   * to calling it with a doubled number.
   *
   * <p>This method has no effect on the result of {@link #availableGas}. There is no way to
   * determine how much gas has been consumed by {@link #registerCpuFee}; binders must maintain this
   * information themselves.
   *
   * @param gas The amount of gas to register as used. Must not be negative.
   * @throws RuntimeException if the call results in the contract having consumed more gas than
   *     {@link #availableGas}.
   */
  void registerCpuFee(long gas);
}
