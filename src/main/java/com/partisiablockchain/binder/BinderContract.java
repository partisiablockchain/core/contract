package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Binder contracts binds the actual, deployed contracts to the blockchain.
 *
 * <p>These contracts serves as the glue between the blockchain and the programmed contract.
 *
 * <p>A binder is deployed together with the actual contract, and therefore PBC allows different
 * versions of binders and thereby changes in the API for the contracts, since the binders are
 * runtime loaded and bridge the API differences.
 *
 * <p>Binders are intended to deliverer as a part of PBC.
 *
 * <p>For ZK contracts, all the shared orchestration happens in the binder
 *
 * @param <StateT> the contract state type
 * @param <ContextT> the type of context for the contract call
 * @param <EventT> the type of events that this binder creates
 */
public interface BinderContract<
    StateT extends StateSerializable, ContextT, EventT extends BinderEvent> {

  /**
   * Gets the class for the contract state.
   *
   * @return the class
   */
  Class<StateT> getStateClass();

  /**
   * Execute a creation of a contract.
   *
   * @param context contract context
   * @param rpc invocation data for creation
   * @return initial contract state
   */
  BinderResult<StateT, EventT> create(ContextT context, byte[] rpc);

  /**
   * Execute an invocation on contract.
   *
   * @param context contract context
   * @param state contract state
   * @param rpc invocation data for invocation
   * @return next contract state
   */
  BinderResult<StateT, EventT> invoke(ContextT context, StateT state, byte[] rpc);

  /**
   * Execute a callback for contract.
   *
   * @param context contract context
   * @param state contract state
   * @param callbackContext call back context
   * @param rpc invocation data for callback
   * @return next contract state
   */
  BinderResult<StateT, EventT> callback(
      ContextT context, StateT state, CallbackContext callbackContext, byte[] rpc);

  /**
   * Checks whether a contract is upgradable. Must be called before the upgradeState method.
   * Expected to throw an exception if the upgrade cannot be performed.
   *
   * @param ctx the event context
   * @param state the contract state
   * @param oldContractBytecodeIds the bytecode identifiers of the old contract
   * @param newContractBytecodeIds the bytecode identifiers of the new contract
   * @param rpc the contract upgrade rpc
   * @return permit with contract data if upgrade is allowed
   */
  default ContractUpgradePermit acquireUpgradePermit(
      ContextT ctx,
      StateT state,
      ContractIdentifiers oldContractBytecodeIds,
      ContractIdentifiers newContractBytecodeIds,
      byte[] rpc) {
    throw new UnsupportedOperationException(
        "Binder %s does not implement support for acquireUpgradePermit"
            .formatted(this.getClass().getName()));
  }

  /**
   * Upgrades the state of a contract if upgrade is allowed. Must only be called after a contract
   * upgrade permit has been granted. Expected to throw an exception if the state migration cannot
   * be performed.
   *
   * @param ctx the event context
   * @param oldState the old contract state
   * @param permit the permit with additional data issued by the old contract
   * @param rpc the contract upgrade rpc
   * @return new contract state
   */
  default StateT upgradeState(
      ContextT ctx, StateAccessor oldState, ContractUpgradePermit permit, byte[] rpc) {
    throw new UnsupportedOperationException(
        "Binder %s does not implement support for upgradeState"
            .formatted(this.getClass().getName()));
  }
}
