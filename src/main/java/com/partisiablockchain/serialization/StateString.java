package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/**
 * Simple example contract state with a {@link String} value.
 *
 * <p>Mainly useful for examples and tests, but sometimes used in contract states.
 *
 * <p><b>WARNING</b>: This class is slated for deprecation. You can replace this class easily by
 * including: {@code @Immutable record MyStateString(String value) implements StateSerializable {}}
 */
@Immutable
public final class StateString implements StateSerializable {

  private final String value;

  /** Create state string of null. */
  public StateString() {
    value = null;
  }

  /**
   * Create state string.
   *
   * @param value string value
   */
  public StateString(String value) {
    this.value = value;
  }

  /**
   * Get the value.
   *
   * @return value
   */
  public String value() {
    return value;
  }
}
