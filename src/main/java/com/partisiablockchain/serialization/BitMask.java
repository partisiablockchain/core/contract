package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Arrays;
import java.util.BitSet;

final class BitMask {

  private final BitSet bitSet = new BitSet();
  private int size = 0;

  void write(boolean bit) {
    bitSet.set(size, bit);
    size++;
  }

  byte[] asBytes() {
    byte[] value = bitSet.toByteArray();
    int bytesNeeded = bytesNeededForBits(size);
    return Arrays.copyOfRange(value, 0, bytesNeeded);
  }

  static int bytesNeededForBits(int bitCount) {
    if (bitCount == 0) {
      return 0;
    } else {
      return 1 + (bitCount - 1) / 8;
    }
  }
}
