package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

/** A decorated storage that remembers the hashes written and read. */
public final class HashHistoryStorage implements StateStorage {

  private final StateStorage storage;
  private final Set<Hash> hashesRead;

  /**
   * Create decorated storage with history.
   *
   * @param storage decorated storage
   */
  public HashHistoryStorage(StateStorage storage) {
    this.storage = storage;
    this.hashesRead = new HashSet<>();
  }

  @Override
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    storage.write(hash, writer);
    return hashesRead.add(hash);
  }

  @Override
  public <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
    hashesRead.add(hash);
    return storage.read(hash, reader);
  }
}
