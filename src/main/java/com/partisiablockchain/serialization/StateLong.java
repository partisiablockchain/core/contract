package com.partisiablockchain.serialization;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/**
 * Simple example contract state with a {@link Long} value.
 *
 * <p>Mainly useful for examples and tests, but sometimes used in contract states.
 *
 * <p><b>WARNING</b>: This class is slated for deprecation. You can replace this class easily by
 * including: {@code @Immutable record MyStateLong(long value) implements StateSerializable {}}
 */
@Immutable
public final class StateLong implements StateSerializable {

  private final long value;

  /** Create state long of zero. */
  public StateLong() {
    value = 0;
  }

  /**
   * Create state long.
   *
   * @param value long value
   */
  public StateLong(long value) {
    this.value = value;
  }

  /**
   * Get the value.
   *
   * @return value
   */
  public long value() {
    return value;
  }
}
