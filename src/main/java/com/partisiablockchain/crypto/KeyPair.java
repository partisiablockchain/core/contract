package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.crypto.Curve.CURVE;

import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.signers.DSAKCalculator;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.crypto.util.DigestFactory;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;

/** Class for elliptic curve signature related functions. */
public final class KeyPair implements SignatureProvider {

  private final BlockchainPublicKey publicKey;
  private final BigInteger privateKey;

  /**
   * Construct a new key pair for the supplied private key.
   *
   * @param privateKey the private key to sign with
   */
  public KeyPair(BigInteger privateKey) {
    if (privateKey.bitLength() > CURVE.getN().bitLength()) {
      privateKey = privateKey.mod(CURVE.getN());
    }
    this.privateKey = privateKey;
    ECPoint ecPoint = new FixedPointCombMultiplier().multiply(CURVE.getG(), privateKey);
    this.publicKey = new BlockchainPublicKey(ecPoint);
  }

  /** Construct a new KeyPair with a randomly generated private key. */
  public KeyPair() {
    this(new BigInteger(CURVE.getN().bitLength(), new SecureRandom()));
  }

  static boolean requiresNormalization(BigInteger s) {
    return s.compareTo(Curve.HALF_CURVE_ORDER) > 0;
  }

  /**
   * Calculate the recovery ID, normalize s, and create a signature containing the recovery ID and
   * normalized signature.
   *
   * <p>The recovery ID that is calculated does not account for original x-coordinates of the random
   * point on the elliptic curve being larger than the order of the subgroup (in which case the
   * second to LSB of the recovery ID should be set to 1). This is justified because the probability
   * that this happens is less than 2^{-126} The calculated recovery ID returned by this code will
   * therefore be either 0 or 1 depending only on whether the s parameter of the ECDSA signature
   * requires normalization and which of the two y-coordinates for the random point was used.
   *
   * @param r The first part of the ECDSA signature.
   * @param s The second part of the ECDSA signature.
   * @param k The secret nonce used to calculate r and s.
   * @return The signature consisting of both the newly calculated recovery ID, r, and s.
   */
  static Signature addRecoveryIdAndNormalizeS(BigInteger r, BigInteger s, BigInteger k) {

    byte recoveryId = 0;

    FixedPointCombMultiplier basePointMultiplier = new FixedPointCombMultiplier();
    ECPoint p = basePointMultiplier.multiply(CURVE.getG(), k).normalize();

    // Set the LSB of the recovery ID and normalize if s requires normalization.
    BigInteger y = p.getAffineYCoord().toBigInteger();
    if (requiresNormalization(s)) {
      recoveryId += (byte) 1;
      s = CURVE.getN().subtract(s);
    }
    // Set the LSB of the recovery ID if negative y-coordinate was used (determined by parity) and s
    // was not normalized
    if (y.mod(BigInteger.TWO).equals(BigInteger.ONE)) {
      recoveryId = (byte) (recoveryId ^ 1);
    }

    return new Signature(recoveryId, r, s);
  }

  @Override
  public Signature sign(Hash messageHash) {
    NonceProducerMemoryDecorator nonceProducer =
        new NonceProducerMemoryDecorator(new HMacDSAKCalculator(DigestFactory.createSHA256()));
    ECDSASigner ecdsaSigner = new ECDSASigner(nonceProducer);
    ECPrivateKeyParameters privKey = new ECPrivateKeyParameters(privateKey, CURVE);
    ecdsaSigner.init(true, privKey);
    BigInteger[] bigIntegers = ecdsaSigner.generateSignature(messageHash.getBytes());
    BigInteger r = bigIntegers[0];
    BigInteger s = bigIntegers[1];
    BigInteger k = nonceProducer.getLastK();

    return addRecoveryIdAndNormalizeS(r, s, k);
  }

  /**
   * Get the public key.
   *
   * @return public key
   */
  public BlockchainPublicKey getPublic() {
    return publicKey;
  }

  /**
   * Get the private key.
   *
   * @return private key
   */
  @SuppressWarnings("WeakerAccess")
  public BigInteger getPrivateKey() {
    return privateKey;
  }

  /**
   * Wrapper that adds memory to a DSAKCalculators such that the last provided K can be retrieved.
   */
  static final class NonceProducerMemoryDecorator implements DSAKCalculator {

    private final DSAKCalculator innerNonceProducer;

    private BigInteger lastK;

    /**
     * Constructs a nonce producer with memory given a nonce producer.
     *
     * @param innerNonceProducer the inner none producer it should be based on
     */
    public NonceProducerMemoryDecorator(DSAKCalculator innerNonceProducer) {
      this.lastK = null;
      this.innerNonceProducer = innerNonceProducer;
    }

    @Override
    public boolean isDeterministic() {
      return innerNonceProducer.isDeterministic();
    }

    @Override
    public void init(BigInteger n, SecureRandom random) {
      innerNonceProducer.init(n, random);
    }

    @Override
    public void init(BigInteger n, BigInteger d, byte[] message) {
      innerNonceProducer.init(n, d, message);
    }

    /** The next K is retrieved from the inner DSAKCalculator and stored before it is returned. */
    @Override
    public BigInteger nextK() {
      BigInteger k = innerNonceProducer.nextK();
      lastK = new BigInteger(k.toByteArray());
      return k;
    }

    /**
     * Getter for the latest used nonce.
     *
     * @return last used nonce
     */
    public BigInteger getLastK() {
      return lastK;
    }
  }
}
