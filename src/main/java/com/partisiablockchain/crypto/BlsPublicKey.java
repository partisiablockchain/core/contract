package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.bls.G2;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** A BLS public key which can be aggregated with other public keys. */
@Immutable
public final class BlsPublicKey implements DataStreamSerializable, StateSerializable {

  @SuppressWarnings("Immutable")
  private final G2 publicKeyValue;

  /**
   * Read BLS public key from stream. Assumes the key is serialized in compressed format.
   *
   * @param stream to read from
   * @return read BLS public key
   */
  public static BlsPublicKey read(SafeDataInputStream stream) {
    return new BlsPublicKey(stream.readBytes(G2.BYTE_SIZE));
  }

  /**
   * Read BLS public key from stream without verification. This is not safe to use on untrusted
   * inputs.
   *
   * @param stream to read from
   * @return read BLS public key
   */
  public static BlsPublicKey readUnsafe(SafeDataInputStream stream) {
    return createUnsafe(stream.readBytes(G2.BYTE_SIZE));
  }

  /**
   * Read BLS public key from stream. The key is serialized in uncompressed format.
   *
   * @param stream to read from
   * @return read BLS public key
   */
  public static BlsPublicKey readUncompressed(SafeDataInputStream stream) {
    return new BlsPublicKey(stream.readBytes(2 * G2.BYTE_SIZE));
  }

  BlsPublicKey(G2 publicKeyValue) {
    this.publicKeyValue = publicKeyValue;
  }

  /**
   * Create a new public key from the supplied bytes.
   *
   * @param publicKeyBytes the bytes representing a compressed public key
   */
  public BlsPublicKey(byte[] publicKeyBytes) {
    G2 publicKey = G2.create(publicKeyBytes);
    if (publicKey.isPointAtInfinity()) {
      throw new IllegalArgumentException("Public keys cannot point at infinity");
    }
    this.publicKeyValue = publicKey;
  }

  /**
   * Add a new public key to this aggregate public key. See {@link
   * BlsSignature#addSignature(BlsSignature)} for a description of how signature aggregation works.
   *
   * @param publicKey the public key
   * @return a new aggregate public key.
   */
  public BlsPublicKey addPublicKey(BlsPublicKey publicKey) {
    return new BlsPublicKey(getPublicKeyValue().addPoint(publicKey.getPublicKeyValue()));
  }

  /**
   * Get the point of this public key.
   *
   * @return the inner point
   */
  public G2 getPublicKeyValue() {
    return publicKeyValue;
  }

  private byte[] getPublicKeyBytes() {
    return publicKeyValue.serialize(true);
  }

  /**
   * Read a public key without validating the G2 point. This is not safe to use on untrusted inputs.
   *
   * @param in the input to read from
   * @return the deserialized object
   */
  public static BlsPublicKey createUnsafe(byte[] in) {
    return new BlsPublicKey(G2.createUnsafe(in));
  }

  /**
   * Writes the object to a {@link SafeDataOutputStream}.
   *
   * @param stream the destination stream for this object
   */
  @Override
  public void write(SafeDataOutputStream stream) {
    stream.write(getPublicKeyBytes());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BlsPublicKey that = (BlsPublicKey) o;
    return getPublicKeyValue().equals(that.getPublicKeyValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getPublicKeyValue());
  }
}
