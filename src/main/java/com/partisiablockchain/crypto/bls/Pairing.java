package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;

final class Pairing {

  static final BigInteger BLS_X = new BigInteger("d201000000010000", 16);

  private static final Fq2 two = Fq2.createOne().add(Fq2.createOne());

  private static final BigInteger orderBig =
      new BigInteger(
          "52435875175126190479447740508185965837690552500527637822603658699938581184513");

  /** The order of the subgroup G1. */
  static final NafEncoded order = NafEncoded.create(orderBig);

  private static final Fq2 inverseTwo = two.invert();

  private Pairing() {}

  /**
   * Computes the pairing function e(p, q) -> t with t an element of Gt. This method computes the
   * ate pairing with respect to optimization in terms on sparse multiplication inside the Miller
   * algorithm and reduce further the cost of line evaluation formulas in affine and projective
   * homogeneous coordinates. Uses https://github.com/paulmillr/noble-bls12-381 as reference.
   *
   * @param p the first argument, a point in G1
   * @param q the second argument, a point in G2
   * @return a point t in Gt.
   */
  static Gt computePairing(G1 p, G2 q) {
    return finalExponentiation(millerLoop(p, q));
  }

  /**
   * Computes the miller loop with q containing a precomputation of point doubling and additions.
   *
   * @param p G1 point to pair
   * @param ellCoeffs containing precomputed points
   * @return Fq12 element as result of millerLoop
   */
  static Fq12 millerLoop(G1 p, Fq2[][] ellCoeffs) {
    Fq12 result = Fq12.createOne();
    Fq[] paf = p.toAffine();
    for (int j = 0, i = BLS_X.bitLength() - 2; i >= 0; i--, j++) {
      result = lineEvaluation(result, ellCoeffs[j], paf);
      if (BLS_X.testBit(i)) {
        j++;
        result = lineEvaluation(result, ellCoeffs[j], paf);
      }
      result = result.square();
    }
    return result;
  }

  /**
   * Computes the miller loop.
   *
   * @param p G1 point to pair
   * @param q G2 point to pair
   * @return Fq12 element as result of millerLoop
   */
  static Fq12 millerLoop(G1 p, G2 q) {
    // e(P, \inf) = e(\inf, Q) = 1
    if (p.isPointAtInfinity() || q.isPointAtInfinity()) {
      return Fq12.createOne();
    }
    int bits = BLS_X.bitLength();
    Fq[] paf = p.toAffine();
    Fq2[] qaf = q.toAffine();
    Fq2[] rpro = new Fq2[] {qaf[0], qaf[1], Fq2.createOne()};
    Fq12 result = Fq12.createOne();
    for (int i = bits - 2; i >= 0; i--) {
      // R = 2R
      Fq2[][] pointAndCoeff = doublePointInTwist(rpro);
      rpro = pointAndCoeff[0];
      Fq2[] coeff = pointAndCoeff[1];
      // f = f · l_RR(P)
      result = lineEvaluation(result, coeff, paf);
      if (BLS_X.testBit(i)) {
        // R = R + Q
        pointAndCoeff = addPointInTwist(rpro, qaf);
        rpro = pointAndCoeff[0];
        coeff = pointAndCoeff[1];
        // f = f · l_RQ(P)
        result = lineEvaluation(result, coeff, paf);
      }
      result = result.square();
    }
    return result;
  }

  /**
   * Returns a G2 point that is precomputed. This is done by doing the add point and double point
   * operations in advance.
   *
   * @param point point to precompute
   * @return precomputed point
   */
  static Fq2[][] prepareG2PointForPairing(G2 point) {
    Fq2[] qaf = point.toAffine();
    Fq2[] rpro = new Fq2[] {qaf[0], qaf[1], Fq2.createOne()};
    // There is 63 iterations in the for loop and 5 bits are set in BLS_X = 68 length needed.
    Fq2[][] result = new Fq2[68][];
    for (int j = 0, i = BLS_X.bitLength() - 2; i >= 0; i--, j++) {
      Fq2[][] pointAndCoeff = doublePointInTwist(rpro);
      rpro = pointAndCoeff[0];
      result[j] = pointAndCoeff[1];
      if (BLS_X.testBit(i)) {
        j++;
        pointAndCoeff = addPointInTwist(rpro, qaf);
        rpro = pointAndCoeff[0];
        result[j] = pointAndCoeff[1];
      }
    }
    return result;
  }

  /**
   * Uses the formula from sec. 4.3 https://eprint.iacr.org/2013/722.pdf.
   *
   * @param rpro homogeneous coordinates (R)
   * @param qaf affine coordinates (Q)
   * @return T+Q and coefficients for line evaluation.
   */
  static Fq2[][] addPointInTwist(Fq2[] rpro, Fq2[] qaf) {
    // A = Y2 · Z1
    Fq2 a = qaf[1].multiply(rpro[2]);
    // B = X2 · Z1
    Fq2 b = qaf[0].multiply(rpro[2]);
    // θ = Y1 - A
    Fq2 theta = rpro[1].subtract(a);
    // λ = X1 - B
    Fq2 lambda = rpro[0].subtract(b);
    // C = θ²
    Fq2 c = theta.square();
    // D = λ²
    Fq2 d = lambda.square();
    // E = λ^3
    Fq2 e = d.multiply(lambda);
    // F = Z1 · C
    Fq2 f = rpro[2].multiply(c);
    // G = X1 · D
    Fq2 g = d.multiply(rpro[0]);
    // H = E + F - 2G
    Fq2 h = e.add(f).subtract(g.add(g));
    // X3 = λH
    Fq2 rx = lambda.multiply(h);
    // I = Y1 · E
    Fq2 i = e.multiply(rpro[1]);
    // Y3 = θ(G - H) - I
    Fq2 ry = g.subtract(h).multiply(theta).subtract(i);
    // Z3 = Z1 · E
    Fq2 rz = rpro[2].multiply(e);
    // J = θ·X2 - λ·Y2
    Fq2 j = theta.multiply(qaf[0]).subtract(lambda.multiply(qaf[1]));
    // Jw^3, -θ, λ
    Fq2[] coeff = new Fq2[] {j, theta.negate(), lambda};
    return new Fq2[][] {new Fq2[] {rx, ry, rz}, coeff};
  }

  /**
   * Uses the formula 11 in https://eprint.iacr.org/2013/722.pdf.
   *
   * @param rpro homogeneous coordinates (R)
   * @return T+Q and coefficients for line evaluation.
   */
  static Fq2[][] doublePointInTwist(Fq2[] rpro) {
    Fq2 three = Fq2.createConstant(Fq.createConstant(3));
    // A = XY / 2
    Fq2 a = rpro[0].multiply(rpro[1]).multiply(inverseTwo);
    // B = Ry²
    Fq2 b = rpro[1].square();
    // C = Rz²
    Fq2 c = rpro[2].square();
    // E  = 3b' · C
    Fq2 e = c.multiplyBy3B();
    // F = 3 · E
    Fq2 f = e.multiply(three);
    // X3 = A · (B - F)
    Fq2 rx = b.subtract(f).multiply(a);
    // G = (B + F) / 2
    Fq2 g = b.add(f).multiply(inverseTwo);
    // Y3 = G² - 3E²
    Fq2 ry = g.square().subtract(e.square().multiply(three));
    // H = (Y + Z)² - (B + C)
    Fq2 h = rpro[1].add(rpro[2]).square().subtract(c).subtract(b);
    // Z3 = B · H
    Fq2 rz = b.multiply(h);
    // (E - B)w^3, 3X²w, -H
    Fq2[] coeff = new Fq2[] {e.subtract(b), rpro[0].square().multiply(three), h.negate()};
    return new Fq2[][] {new Fq2[] {rx, ry, rz}, coeff};
  }

  /**
   * Performs the line evaluation.
   *
   * @param result result to multiply onto the line evaluation
   * @param coeff The coefficients for the line evaluation
   * @param paf P in affine
   * @return The tangent line evaluated at P
   */
  static Fq12 lineEvaluation(Fq12 result, Fq2[] coeff, Fq[] paf) {
    return result.mulBy014(
        coeff[0], coeff[1].multiplyWithFq(paf[0]), coeff[2].multiplyWithFq(paf[1]));
  }

  /**
   * We split the exponentiation as follows: <br>
   * (q^12 - 1) / r = (q^6 - 1)·[(q^6 - 1)/Phi(12, q)]·[Phi(12, q)/r] <br>
   * = (q^6 - 1)·(q^2 + 1)·[(q^4 - q^2 + 1)/r] where Phi(12, q) = q^4 - q^2 + 1 is the 12'th
   * cyclotomic polynomial evaluated at q.
   *
   * <p>Let v be the value we wish to raise to (q^12 - 1)/r
   *
   * <p>We can compute the final exponentiation in 4 steps, as result = ((v^(q^6 - 1))^(q^2 + 1))^D
   *
   * <p>t1 = v^(q^6 - 1) = v^(q^6)/v <-- 6 applications of the frobenius, 1 inv, and 1 mult
   *
   * <p>t2 = t1^(q^2 + 1) = t1^(q^2)·t1 <-- 2 applications of the frobenius, and 1 mult
   *
   * <p>res = finalExpHardPart(t2·t1) <-- (t2·t1)^(q^4 - q^2 + 1)/r
   *
   * @param value value to raise to ^((q^12 - 1) / r)
   * @return value^((q^12 - 1) / r)
   */
  static Gt finalExponentiation(Fq12 value) {
    Fq12 t0 = value.frobenius().frobenius().frobenius().frobenius().frobenius().frobenius();
    Fq12 t1 = t0.multiply(value.invert());
    Fq12 t2 = t1.frobenius().frobenius();
    Fq12 res = finalExpHardPart(t2.multiply(t1));
    return new Gt(res);
  }

  /**
   * The "hard" part of the exponentiation is D = (q^4 - q^2 + 1)/r can be rewritten into a
   * polynomial in p of degree 3 as follows from https://eprint.iacr.org/2016/130.pdf chapter 3.<br>
   * Let u be BLS_X: (q^4 - q^2 + 1)/r = λ0 + λ1p + λ2p^2 + λ3p^3 <br>
   * where: λ0 = u^5 - 2u^4 + 2u^2 - u - 3, λ1 = u^4 - 2u^3 + 2u - 1, <br>
   * λ2 = u^3 - 2u^2 + u and λ3 = u^2 - 2u + 1.
   *
   * <p>To compute the polynomial of degree 3, start by computing f^(u^5 - 2u^4 + 2u^2). Notice that
   * the exponent is close to λ0. While calculating this value we also obtain values for the other
   * λ's.
   *
   * <p>Computes f^(u^5-2u^4+2u^2) <-- 5 cyclotomic exponentiations, 2 multiplications and 2
   * cyclotomic squarings result.
   *
   * <p>Result = f^D <-- 3 Frobenius maps and 8 mults by calculating the λ's.
   *
   * <p>Comments on sub results will relate to computed terms in table 1 chapter 3 for the listed
   * article above.
   *
   * @param f value to raise to ^(q^4 - q^2 + 1)/r
   * @return f^(q^4 - q^2 + 1)/r
   */
  private static Fq12 finalExpHardPart(Fq12 f) {
    Fq12 t0 = cyclotomicSquare(f).conjugate(); // <- f^-2
    Fq12 t5 = cyclotomicExp(f); // <- f^u
    Fq12 t1 = cyclotomicSquare(t5); //  <- f^2u
    Fq12 t3 = t0.multiply(t5); // <- f^(u-2)
    t0 = cyclotomicExp(t3); // <- f^(u^2-2u)
    Fq12 t2 = cyclotomicExp(t0); // <- f^(u^3-2u^2)
    Fq12 t4 = cyclotomicExp(t2); // <- f^(u^4-2u^3)
    t4 = t4.multiply(t1); // <- f^(u^4-2u^3+2u)
    t1 = cyclotomicExp(t4); // <- f^(u^5-2u^4+2u^2)
    // We have now done λ's step = f^(u^5-2u^4+2u^2)
    // Calculating λ0
    t3 = t3.conjugate(); // <- f^(-u+2)
    t1 = t3.multiply(t1); // <- f^(u^5-2u^4+2u^2-u+2)
    t1 = t1.multiply(f); // <- f^(u^5-2u^4+2u^2-u+3)
    // Calculating λ3
    t3 = f.conjugate(); // t3 <- f^-1
    t0 = t0.multiply(f); // t0 <- f^(u^2-2u-1)
    // λ3p^3
    t0 = t0.frobenius().frobenius().frobenius();
    // Calculating λ1
    t4 = t4.multiply(t3); // <- f^(u^4-2u^3+2u-1)
    // λ1p
    t4 = t4.frobenius();
    // Calculating λ2
    t5 = t5.multiply(t2); // <- f^(u^3-2u^2+u)
    // λ2p^2
    t5 = t5.frobenius().frobenius();
    // Calculating f^(λ0+λ1p+λ2p^2+λ3p^3)
    t5 = t5.multiply(t0); // <- f^(λ2p^2+λ3p^3)
    t5 = t5.multiply(t4); // <- f^(λ1p+λ2p^2+λ3p^3)
    return t5.multiply(t1); // <- f^(q^4 - q^2 + 1)/r
  }

  /**
   * Computes value^(-BLS_X). It uses square and multiply technique. The conjugate is there because
   * the BLS_X constant is supposed to be negative (cf. t in section 4.2.1 in
   * https://datatracker.ietf.org/doc/draft-irtf-cfrg-pairing-friendly-curves/. We have BLS_X = -t).
   * Computing value^-1 in the cyclotomic subgroup is the same as a conjugation (as mentioned in
   * section 3 in https://eprint.iacr.org/2016/130.pdf)
   *
   * @param value value to raise to the <code>-BLS_X</code>'th power
   * @return value^(-BLS_X)
   */
  private static Fq12 cyclotomicExp(Fq12 value) {
    Fq12 tmp = value;
    for (int i = BLS_X.bitLength() - 2; i >= 0; i--) {
      tmp = cyclotomicSquare(tmp);
      if (BLS_X.testBit(i)) {
        tmp = tmp.multiply(value);
      }
    }
    return tmp.conjugate();
  }

  /**
   * Cyclotomic squaring as per by https://eprint.iacr.org/2010/354.pdf algorithm 24.
   *
   * @param value value to square
   * @return value squared
   */
  private static Fq12 cyclotomicSquare(Fq12 value) {
    Fq2 z0 = value.a0.a0;
    Fq2 z4 = value.a0.a1;
    Fq2 z3 = value.a0.a2;
    Fq2 z2 = value.a1.a0;
    Fq2 z1 = value.a1.a1;
    Fq2 z5 = value.a1.a2;

    Fq2[] t0t1 = fq4Square(z0, z1);
    Fq2 t0 = t0t1[0];
    Fq2 t1 = t0t1[1];

    Fq2[] t2t3 = fq4Square(z2, z3);
    Fq2 t2 = t2t3[0];
    Fq2 t3 = t2t3[1];

    Fq2[] t4t5 = fq4Square(z4, z5);
    Fq2 t4 = t4t5[0];
    Fq2 t5 = t4t5[1];
    Fq2 t6 = t5.multiplyByNonResidue();

    return new Fq12(
        new Fq6(
            t0.subtract(z0).multiply(two).add(t0),
            t2.subtract(z4).multiply(two).add(t2),
            t4.subtract(z3).multiply(two).add(t4)),
        new Fq6(
            t6.add(z2).multiply(two).add(t6),
            t1.add(z1).multiply(two).add(t1),
            t3.add(z5).multiply(two).add(t3)));
  }

  private static Fq2[] fq4Square(Fq2 a, Fq2 b) {
    Fq2 a2 = a.square();
    Fq2 b2 = b.square();
    return new Fq2[] {
      b2.multiplyByNonResidue().add(a2), a.add(b).square().subtract(a2).subtract(b2)
    };
  }
}
