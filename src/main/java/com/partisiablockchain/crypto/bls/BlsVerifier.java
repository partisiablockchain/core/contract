package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;

/** Verifier for BLS signatures. */
public final class BlsVerifier {

  private BlsVerifier() {}

  private static final Fq2[][] preparedNegatedG2G =
      Pairing.prepareG2PointForPairing(G2.G.negatePoint());

  /**
   * Verifies a (possible aggregate) BLS signature.
   *
   * @param message the message
   * @param signature the (aggregate) signature
   * @param publicKey the (aggregate) key used to sign the message
   * @return true if the signature is valid and false otherwise.
   */
  public static boolean verify(Hash message, BlsSignature signature, BlsPublicKey publicKey) {
    G1 hashedMessage = G1.hash(message);
    // valid if e(H(M), pk) == e(signature, G).
    // This uses the same trick as zcash: Recall that e(P, Q) = m(P, Q)^x for some giant x.
    // Observe that
    //   e(H(M), pk) = e(signature, G)
    // is equivalent to the check
    //   e(H(M), pk)/e(signature, G) = e(H(M), pk)e(signature, -G)
    //                               = 1
    // because e is bilinear, this is the same as writing
    //   e(H(M), pk)e(signature, -G) = (m(H(M), pk)^x·m(signature, -G)^x)
    //                               = (m(H(M), pk)·m(signature, -G))^x
    // so we can get away with only performing one exponentiation.
    Fq12 lhs = Pairing.millerLoop(hashedMessage, publicKey.getPublicKeyValue());
    Fq12 rhs = Pairing.millerLoop(signature.getSignatureValue(), preparedNegatedG2G);
    return Pairing.finalExponentiation(lhs.multiply(rhs)).equals(Gt.createOne());
  }
}
