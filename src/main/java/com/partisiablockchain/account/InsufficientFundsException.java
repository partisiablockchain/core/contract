package com.partisiablockchain.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;

/** Thrown whenever there is an insufficient amount of funds. */
public final class InsufficientFundsException extends RuntimeException {

  private static final long serialVersionUID = 2937139177299494326L;

  /**
   * Create insufficient funds exception.
   *
   * @param address indicates the account with too few funds
   */
  public InsufficientFundsException(BlockchainAddress address) {
    super("Insufficient funds for " + address);
  }
}
