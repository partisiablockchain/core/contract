package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ShareWriteReadTest {

  @Test
  public void testSerialization() {
    ShareWriter shareWriter = ShareWriter.create();
    shareWriter.write(true);
    shareWriter.write(false);
    shareWriter.write(false);
    shareWriter.write(true);
    shareWriter.write(-1, 10);
    shareWriter.write(0, 45);
    shareWriter.write(true);
    shareWriter.write(0b111, 3);
    byte[] bytes = shareWriter.toBytes();

    int shareBitLength = 4 + 10 + 45 + 1 + 3;
    Assertions.assertThat(bytes)
        .hasSize(ZkClosed.getByteLength(shareBitLength))
        .containsExactly(-7, 63, 0, 0, 0, 0, 0, 120);

    ShareReader shareReader = new ShareReader(bytes, shareBitLength);
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readBoolean()).isFalse();
    Assertions.assertThat(shareReader.readBoolean()).isFalse();
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readLong(10)).isEqualTo(1023L);
    Assertions.assertThat(shareReader.readLong(45)).isEqualTo(0);
    Assertions.assertThat(shareReader.readBoolean()).isTrue();
    Assertions.assertThat(shareReader.readLong(3)).isEqualTo(7);

    shareReader = new ShareReader(bytes, shareBitLength);
    Assertions.assertThat(shareReader.readSignedLong(4)).isEqualTo(-7);
    Assertions.assertThat(shareReader.readSignedLong(10)).isEqualTo(-1);
    Assertions.assertThat(shareReader.readSignedLong(45)).isEqualTo(0);
    Assertions.assertThat(shareReader.readSignedLong(1)).isEqualTo(-1);
    Assertions.assertThat(shareReader.readSignedLong(3)).isEqualTo(-1);
  }
}
