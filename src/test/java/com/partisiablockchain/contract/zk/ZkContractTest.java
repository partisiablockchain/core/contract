package com.partisiablockchain.contract.zk;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateString;
import com.secata.stream.SafeDataInputStream;
import java.util.List;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ZkContractTest {

  private final ZkContract<StateString, StateString, ZkClosedTest, StateString> handler =
      new TestContract(new StateString("No type bounds"));

  @Test
  public void getComputation() {
    Assertions.assertThat(handler.getComputation()).isNotNull();
    Assertions.assertThat(handler.getComputation().value()).isEqualTo("No type bounds");
  }

  @Test
  public void onCreate() {
    assertIsNull(state -> handler.onCreate(null, null, null));
  }

  @Test
  public void onCallback() {
    assertIsUnchanged(state -> handler.onCallback(null, null, state, null, null));
  }

  @Test
  public void onOpenInput() {
    assertIsUnchanged(state -> handler.onOpenInput(null, null, state, null));
  }

  @Test
  public void onSecretInput() {
    assertIsUnchanged(state -> handler.onSecretInput(null, null, state, null, null));
  }

  @Test
  public void onVariableInputted() {
    assertIsUnchanged(state -> handler.onVariableInputted(null, null, state, 0));
  }

  @Test
  public void onVariableRjected() {
    assertIsUnchanged(state -> handler.onVariableRejected(null, null, state, 0));
  }

  @Test
  public void onComputeComplete() {
    assertIsUnchanged(state -> handler.onComputeComplete(null, null, state, null));
  }

  @Test
  public void onUserVariablesOpened() {
    assertIsUnchanged(state -> handler.onUserVariablesOpened(null, null, state, null, null));
  }

  @Test
  public void onVariablesOpened() {
    assertIsUnchanged(state -> handler.onVariablesOpened(null, null, state, null));
  }

  @Test
  void onAttestationComplete() {
    assertIsUnchanged(state -> handler.onAttestationComplete(null, null, state, null));
  }

  private void assertIsUnchanged(Function<StateString, StateString> updater) {
    StateString testData = new StateString("My private test data");
    StateString apply = updater.apply(testData);
    Assertions.assertThat(apply).isNotNull();
    Assertions.assertThat(apply.value()).isEqualTo(testData.value());
  }

  private void assertIsNull(Function<StateString, StateString> updater) {
    StateString testData = new StateString("Something");
    StateString apply = updater.apply(testData);
    Assertions.assertThat(apply).isNull();
  }

  private static final class TestContract
      extends ZkContract<StateString, StateString, ZkClosedTest, StateString> {

    TestContract(StateString computation) {
      super(computation);
    }

    private void check(
        ZkContractContext context,
        StateString state,
        ZkState<StateString, ZkClosedTest> zkState,
        SafeDataInputStream rpc,
        boolean isStateNull) {
      Assertions.assertThat(context).isNull();
      Assertions.assertThat(zkState).isNull();
      if (isStateNull) {
        Assertions.assertThat(state).isNull();
      } else {
        Assertions.assertThat(state).isNotNull();
      }
      Assertions.assertThat(rpc).isNull();
    }

    @Override
    public StateString onCreate(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        SafeDataInputStream invocation) {
      StateString state = super.onCreate(context, zkState, invocation);
      check(context, state, zkState, invocation, true);
      return state;
    }

    @Override
    public StateString onCallback(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        CallbackContext callbackContext,
        SafeDataInputStream invocation) {
      StateString stateResult =
          super.onCallback(context, zkState, state, callbackContext, invocation);
      check(context, state, zkState, invocation, false);
      return stateResult;
    }

    @Override
    public StateString onOpenInput(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        SafeDataInputStream invocation) {
      check(context, state, zkState, invocation, false);
      state = super.onOpenInput(context, zkState, state, invocation);
      check(context, state, zkState, invocation, false);
      return state;
    }

    @Override
    public StateString onSecretInput(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        SecretInputBuilder<StateString> input,
        SafeDataInputStream invocation) {
      check(context, state, zkState, invocation, false);
      state = super.onSecretInput(context, zkState, state, input, invocation);
      check(context, state, zkState, invocation, false);
      return state;
    }

    @Override
    public StateString onVariableInputted(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        int variableId) {
      check(context, state, zkState, null, false);
      state = super.onVariableInputted(context, zkState, state, variableId);
      check(context, state, zkState, null, false);
      return state;
    }

    @Override
    public StateString onVariableRejected(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        int variableId) {
      check(context, state, zkState, null, false);
      state = super.onVariableRejected(context, zkState, state, variableId);
      check(context, state, zkState, null, false);
      return state;
    }

    @Override
    public StateString onComputeComplete(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        List<Integer> createdVariables) {
      check(context, state, zkState, null, false);
      state = super.onComputeComplete(context, zkState, state, createdVariables);
      check(context, state, zkState, null, false);
      return state;
    }

    @Override
    public StateString onUserVariablesOpened(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        List<Integer> openedVariables,
        SafeDataInputStream invocation) {
      check(context, state, zkState, invocation, false);
      state = super.onUserVariablesOpened(context, zkState, state, openedVariables, invocation);
      check(context, state, zkState, invocation, false);
      return state;
    }

    @Override
    public StateString onVariablesOpened(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        List<Integer> openedVariables) {
      check(context, state, zkState, null, false);
      state = super.onVariablesOpened(context, zkState, state, openedVariables);
      check(context, state, zkState, null, false);
      return state;
    }

    @Override
    public StateString onAttestationComplete(
        ZkContractContext context,
        ZkState<StateString, ZkClosedTest> zkState,
        StateString state,
        Integer attestationId) {
      check(context, state, zkState, null, false);
      state = super.onAttestationComplete(context, zkState, state, attestationId);
      check(context, state, zkState, null, false);
      return state;
    }
  }

  @Immutable
  private static final class ZkClosedTest implements ZkClosed<StateString> {

    @Override
    public int getId() {
      return 0;
    }

    @Override
    public BlockchainAddress getOwner() {
      return null;
    }

    @Override
    public StateString getInformation() {
      return null;
    }

    @Override
    public boolean isSealed() {
      return false;
    }

    @Override
    public byte[] getOpenValue() {
      return new byte[0];
    }

    @Override
    public int getShareBitLength() {
      return 0;
    }
  }
}
