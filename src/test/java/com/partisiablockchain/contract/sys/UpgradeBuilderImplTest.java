package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.FunctionUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class UpgradeBuilderImplTest {

  private final ContractEventGroup sender = new ContractEventGroup();
  private static final Hash EMPTY_HASH = Hash.create(FunctionUtility.noOpConsumer());
  private static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_PUBLIC, EMPTY_HASH);

  @Test
  public void sendUpgrade() {
    new UpgradeBuilderImpl(sender::add, CONTRACT_ADDRESS)
        .withNewBinderJar(new byte[11])
        .withNewContractJar(new byte[22])
        .withNewAbi(new byte[44])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .allocateCost(1234)
        .send();

    assertSentUpgrade(1234L, true);
  }

  @Test
  public void sendUpgradeWithRemaining() {
    new UpgradeBuilderImpl(sender::add, CONTRACT_ADDRESS)
        .withNewBinderJar(new byte[11])
        .withNewContractJar(new byte[22])
        .withNewAbi(new byte[44])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .allocateRemainingCost()
        .send();

    assertSentUpgrade(null, true);
  }

  @Test
  public void sendUpgradeFromContract() {
    new UpgradeBuilderImpl(sender::add, CONTRACT_ADDRESS)
        .withNewBinderJar(new byte[11])
        .withNewContractJar(new byte[22])
        .withNewAbi(new byte[44])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .allocateCost(1234)
        .sendFromContract();

    assertSentUpgrade(1234L, false);
  }

  @Test
  public void nullChecks() {
    Assertions.assertThatThrownBy(() -> new UpgradeBuilderImpl(sender::add, null));
    Assertions.assertThatThrownBy(
            () ->
                new UpgradeBuilderImpl(sender::add, CONTRACT_ADDRESS).withNewBinderJar(null).send())
        .isInstanceOf(NullPointerException.class);
    Assertions.assertThatThrownBy(
            () ->
                new UpgradeBuilderImpl(sender::add, CONTRACT_ADDRESS)
                    .withNewContractJar(null)
                    .send())
        .isInstanceOf(NullPointerException.class);
  }

  private void assertSentUpgrade(Long cost, boolean originalSender) {
    ContractEventUpgrade contractEvent = (ContractEventUpgrade) sender.contractEvents.get(0);
    Upgrade upgrade = contractEvent.upgrade;

    Assertions.assertThat(upgrade.contract).isEqualTo(CONTRACT_ADDRESS);
    Assertions.assertThat(upgrade.newBinderJar).hasSize(11);
    Assertions.assertThat(upgrade.newContractJar).hasSize(22);
    Assertions.assertThat(upgrade.newAbi).hasSize(44);
    Assertions.assertThat(contractEvent.allocatedCost).isEqualTo(cost);
    Assertions.assertThat(contractEvent.originalSender).isEqualTo(originalSender);
  }
}
