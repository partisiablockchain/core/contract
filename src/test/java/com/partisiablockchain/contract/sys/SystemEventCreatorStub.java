package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.InteractionBuilder;

final class SystemEventCreatorStub implements SystemEventCreator {

  @Override
  public void createAccount(BlockchainAddress address) {}

  @Override
  public void createShard(String shardId) {}

  @Override
  public void removeShard(String shardId) {}

  @Override
  public void updateLocalAccountPluginState(LocalPluginStateUpdate update) {}

  @Override
  public void updateContextFreeAccountPluginState(String shardId, byte[] rpc) {}

  @Override
  public void updateGlobalAccountPluginState(GlobalPluginStateUpdate update) {}

  @Override
  public void updateAccountPlugin(byte[] pluginJar, byte[] rpc) {}

  @Override
  public void updateLocalConsensusPluginState(LocalPluginStateUpdate update) {}

  @Override
  public void updateGlobalConsensusPluginState(GlobalPluginStateUpdate update) {}

  @Override
  public void updateConsensusPlugin(byte[] pluginJar, byte[] rpc) {}

  @Override
  public void updateRoutingPlugin(byte[] pluginJar, byte[] rpc) {}

  @Override
  public void removeContract(BlockchainAddress contract) {}

  @Override
  public void exists(BlockchainAddress address) {}

  @Override
  public void setFeature(String key, String value) {}

  @Override
  public void upgradeSystemContract(
      byte[] contractJar, byte[] binderJar, byte[] rpc, BlockchainAddress contractAddress) {}

  @Override
  public void upgradeSystemContract(
      byte[] contractJar,
      byte[] binderJar,
      byte[] abi,
      byte[] rpc,
      BlockchainAddress contractAddress) {}

  @Override
  public DeployBuilder deployContract(BlockchainAddress contract) {
    return null;
  }

  @Override
  public InteractionBuilder invoke(BlockchainAddress contract) {
    return null;
  }
}
