package com.partisiablockchain.contract.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.immutable.FixedList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SysContractContextTest {

  @Test
  void sharedObjectStoreNotSupportedDefault() {
    SysContractContextTestImpl context = new SysContractContextTestImpl();
    Assertions.assertThatThrownBy(context::getGlobalSharedObjectStorePluginState)
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Binder does not support the Shared Object Store");
  }

  private static final class SysContractContextTestImpl implements SysContractContext {

    @Override
    public void setResult(DataStreamSerializable result) {}

    @Override
    public BlockchainAddress getContractAddress() {
      return null;
    }

    @Override
    public long getBlockTime() {
      return 0;
    }

    @Override
    public long getBlockProductionTime() {
      return 0;
    }

    @Override
    public BlockchainAddress getFrom() {
      return null;
    }

    @Override
    public Hash getCurrentTransactionHash() {
      return null;
    }

    @Override
    public Hash getOriginalTransactionHash() {
      return null;
    }

    @Override
    public SystemEventCreator getInvocationCreator() {
      return null;
    }

    @Override
    public SystemEventManager getRemoteCallsCreator() {
      return null;
    }

    @Override
    public StateSerializable getGlobalAccountPluginState() {
      return null;
    }

    @Override
    public PluginInteractionCreator getAccountPluginInteractions() {
      return null;
    }

    @Override
    public StateSerializable getGlobalConsensusPluginState() {
      return null;
    }

    @Override
    public PluginInteractionCreator getConsensusPluginInteractions() {
      return null;
    }

    @Override
    public Governance getGovernance() {
      return null;
    }

    @Override
    public String getFeature(String key) {
      return null;
    }

    @Override
    public void registerDeductedByocFees(
        Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {}

    @Override
    public void payInfrastructureFees(long gas, BlockchainAddress target) {}

    @Override
    public void payServiceFees(long gas, BlockchainAddress target) {}
  }
}
