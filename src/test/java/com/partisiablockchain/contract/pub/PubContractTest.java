package com.partisiablockchain.contract.pub;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateString;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PubContractTest {

  private final TestPubContract handler = new TestPubContract();

  @Test
  public void onCreate() {
    Assertions.assertThat(handler.onCreate(null, null)).isNull();
    check(true, false, false, false);
  }

  @Test
  public void onInvoke() {
    Assertions.assertThat(handler.onInvoke(null, new StateString(), null)).isNotNull();
    check(false, true, false, false);
  }

  @Test
  public void onCallback() {
    Hash hash = Hash.create(FunctionUtility.noOpConsumer());
    CallbackContext.ExecutionResult executionResult =
        CallbackContext.createResult(
            hash,
            true,
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(stream -> stream.writeInt(123))));
    CallbackContext callbackContext =
        CallbackContext.create(FixedList.create(List.of(executionResult)));
    Assertions.assertThat(handler.onCallback(null, new StateString(), callbackContext, null))
        .isNotNull();
    check(false, false, true, false);
  }

  private void check(boolean create, boolean invoke, boolean callback, boolean destroy) {
    Assertions.assertThat(handler.create).isEqualTo(create);
    Assertions.assertThat(handler.invoke).isEqualTo(invoke);
    Assertions.assertThat(handler.callback).isEqualTo(callback);
    Assertions.assertThat(handler.destroy).isEqualTo(destroy);
  }

  private static final class TestPubContract extends PubContract<StateString> {

    private boolean create;
    private boolean invoke;
    private boolean callback;
    private boolean destroy;

    @Override
    public StateString onCreate(PubContractContext context, SafeDataInputStream rpc) {
      StateString create = super.onCreate(context, rpc);
      this.create = true;
      check(context, create, rpc, true);
      return create;
    }

    @Override
    public StateString onInvoke(
        PubContractContext context, StateString state, SafeDataInputStream rpc) {
      StateString invoke = super.onInvoke(context, state, rpc);
      this.invoke = true;
      check(context, invoke, rpc, false);
      return invoke;
    }

    @Override
    public StateString onCallback(
        PubContractContext context,
        StateString state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      final StateString callback = super.onCallback(context, state, callbackContext, rpc);
      Hash expected = Hash.create(FunctionUtility.noOpConsumer());
      Assertions.assertThat(callbackContext.results()).hasSize(1);
      CallbackContext.ExecutionResult result = callbackContext.results().get(0);
      Assertions.assertThat(result.eventTransaction()).isEqualTo(expected);
      Assertions.assertThat(result.isSucceeded()).isTrue();
      Assertions.assertThat(result.returnValue().readInt()).isEqualTo(123);
      this.callback = true;
      check(context, callback, rpc, false);
      return callback;
    }

    private void check(
        PubContractContext context,
        StateString state,
        SafeDataInputStream rpc,
        boolean isStateNull) {
      Assertions.assertThat(context).isNull();
      if (isStateNull) {
        Assertions.assertThat(state).isNull();
      } else {
        Assertions.assertThat(state.value()).isNull();
      }
      Assertions.assertThat(rpc).isNull();
    }
  }
}
