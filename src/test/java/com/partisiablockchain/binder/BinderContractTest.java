package com.partisiablockchain.binder;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.serialization.StateSerializable;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BinderContractTest {

  @Test
  void contractUpgradeNotSupportedDefault() {
    BinderContractTestImpl contract = new BinderContractTestImpl();
    ContractIdentifiers oldContractBytecodeIds = new ContractIdentifiers(null, null, null);
    ContractIdentifiers newContractBytecodeIds = new ContractIdentifiers(null, null, null);
    Assertions.assertThatThrownBy(
            () ->
                contract.acquireUpgradePermit(
                    null, null, oldContractBytecodeIds, newContractBytecodeIds, null))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage(
            "Binder com.partisiablockchain.binder.BinderContractTest$BinderContractTestImpl"
                + " does not implement support for acquireUpgradePermit");
    ContractUpgradePermit permit = new ContractUpgradePermit(null);
    Assertions.assertThatThrownBy(() -> contract.upgradeState(null, null, permit, null))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage(
            "Binder com.partisiablockchain.binder.BinderContractTest$BinderContractTestImpl"
                + " does not implement support for upgradeState");
  }

  private static final class BinderContractTestImpl
      implements CommonBinderContract<StateSerializable, Object> {

    @Override
    public Class<StateSerializable> getStateClass() {
      return null;
    }

    @Override
    public BinderResult<StateSerializable, BinderInteraction> create(Object context, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateSerializable, BinderInteraction> invoke(
        Object context, StateSerializable state, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateSerializable, BinderInteraction> callback(
        Object context, StateSerializable state, CallbackContext callbackContext, byte[] rpc) {
      return null;
    }
  }
}
