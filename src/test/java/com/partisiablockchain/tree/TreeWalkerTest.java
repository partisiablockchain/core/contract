package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TreeWalkerTest {

  private static final AvlTree<Integer, Integer> TREE =
      AvlTree.create(Map.of(1, 1, 2, 2, 3, 3, 4, 4));

  /** When creating a tree walker on an EmptyTree, current node and next node both gives null. */
  @Test
  public void emptyTree() {
    AvlTree<Integer, Integer> tree = AvlTree.create();
    TreeWalker<Integer, Integer> walker = new TreeWalker<>(tree);
    Assertions.assertThat(walker.currentNode()).isNull();
    Assertions.assertThat(walker.nextNode()).isNull();
    Assertions.assertThat(walker.nextNode()).isNull();
  }

  /** The tree walker visits the tree in a pre-order. */
  @Test
  void fullPreOrderWalk() {
    TreeWalker<Integer, Integer> walker = new TreeWalker<>(TREE);
    var node1 = (AvlComposite<Integer, Integer>) walker.currentNode();
    Assertions.assertThat(node1).isEqualTo(TREE);

    var node2 = (AvlComposite<Integer, Integer>) walker.nextNode();
    Assertions.assertThat(node2).isEqualTo(node1.getLeft());

    var node3 = (AvlLeaf<Integer, Integer>) walker.nextNode();
    Assertions.assertThat(node3).isEqualTo(node2.getLeft());
    Assertions.assertThat(node3.getKey()).isEqualTo(1);

    var node4 = (AvlLeaf<Integer, Integer>) walker.nextNode();
    Assertions.assertThat(node4).isEqualTo(node2.getRight());
    Assertions.assertThat(node4.getKey()).isEqualTo(2);

    var node5 = (AvlComposite<Integer, Integer>) walker.nextNode();
    Assertions.assertThat(node5).isEqualTo(node1.getRight());

    var node6 = (AvlLeaf<Integer, Integer>) walker.nextNode();
    Assertions.assertThat(node6).isEqualTo(node5.getLeft());
    Assertions.assertThat(node6.getKey()).isEqualTo(3);

    var node7 = (AvlLeaf<Integer, Integer>) walker.nextNode();
    Assertions.assertThat(node7).isEqualTo(node5.getRight());
    Assertions.assertThat(node7.getKey()).isEqualTo(4);

    Assertions.assertThat(walker.nextNode()).isNull();
  }

  /** Current node is the same as the node gotten from previous nextNode. */
  @Test
  void currentNode() {
    TreeWalker<Integer, Integer> walker = new TreeWalker<>(TREE);
    var node1 = walker.nextNode();
    Assertions.assertThat(walker.currentNode()).isEqualTo(node1);
    var node2 = walker.nextNode();
    Assertions.assertThat(walker.currentNode()).isEqualTo(node2);
  }

  /** Skipping children at the root skips the entire tree and returns null. */
  @Test
  void skipRoot() {
    TreeWalker<Integer, Integer> walker = new TreeWalker<>(TREE);
    Assertions.assertThat(walker.skipChildren()).isNull();
    Assertions.assertThat(walker.currentNode()).isNull();
  }

  /** Skipping children skips the subtree and returns the next node after. */
  @Test
  void skipChildren() {
    TreeWalker<Integer, Integer> walker = new TreeWalker<>(TREE);
    var root = (AvlComposite<Integer, Integer>) TREE;
    walker.nextNode();

    AvlTree<Integer, Integer> node = walker.skipChildren();
    Assertions.assertThat(node).isEqualTo(root.getRight());
    Assertions.assertThat(walker.currentNode()).isEqualTo(node);
    Assertions.assertThat(node.getKey()).isEqualTo(3);
  }
}
