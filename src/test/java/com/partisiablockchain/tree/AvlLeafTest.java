package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.StateLong;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AvlLeafTest {

  @Test
  public void accessors() {
    AvlLeaf<Integer, StateLong> leaf = new AvlLeaf<>(10, new StateLong(20));
    Assertions.assertThat(leaf.getValue(11)).isNull();
    Assertions.assertThat(leaf.getValue(10).value()).isEqualTo(20);

    Assertions.assertThat(leaf.maxKeyInTree()).isEqualTo(10);
    Assertions.assertThat(leaf.height()).isEqualTo((byte) 0);
    Assertions.assertThat(leaf.size()).isEqualTo(1);
    Assertions.assertThat(leaf.balance()).isEqualTo(0);
    Assertions.assertThat(leaf.getKey()).isEqualTo(10);
  }
}
