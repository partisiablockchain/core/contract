package com.partisiablockchain.tree;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.HashHistoryStorage;
import com.partisiablockchain.serialization.MemoryStorage;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/** Test of {@link AvlTree}. */
public final class AvlTreeTest {

  private final Random random = new Random(1475);

  /** Underlying storage for {@link AvlTree} storage for tests. */
  private MemoryStorage storage;

  /** Serializer of data for {@link AvlTree} tests. */
  private StateSerializer populate;

  /** Initialize storage and serializer for tests. */
  @BeforeEach
  public void setupStorageForAvlTree() {
    storage = new MemoryStorage();
    populate = new StateSerializer(new HashHistoryStorage(storage), true);
  }

  /** {@link AvlTree} supports {@link Integer} keys. */
  @Test
  public void mapPropertiesIntegerKeys() {
    final Supplier<Map.Entry<Integer, StateLong>> nextEntry =
        () -> Map.entry(random.nextInt(2000), new StateLong(random.nextInt(2000)));
    randomInsertionsAndRemove(nextEntry, false);
    randomInsertionsAndRemove(nextEntry, true);
  }

  /** {@link AvlTree} supports {@link String} keys. */
  @Test
  public void mapPropertiesStringKeys() {
    final Supplier<Map.Entry<String, StateLong>> nextEntry =
        () -> Map.entry("" + random.nextInt(2000), new StateLong(random.nextInt(2000)));
    randomInsertionsAndRemove(nextEntry, false);
    randomInsertionsAndRemove(nextEntry, true);
  }

  /** {@link AvlTree} supports record keys. */
  @Test
  public void mapPropertiesRecordKeys() {
    final Supplier<Map.Entry<MyId, StateLong>> nextEntry =
        () -> Map.entry(new MyId(random.nextInt(2000)), new StateLong(random.nextInt(2000)));
    randomInsertionsAndRemove(nextEntry, false);
    randomInsertionsAndRemove(nextEntry, true);
  }

  /** {@link AvlTree} supports generic record keys. */
  @Test
  public void mapPropertiesGenericRecordKeys() {
    final Supplier<Map.Entry<MyGenericId<StateLong>, StateLong>> nextEntry =
        () ->
            Map.entry(
                new MyGenericId<StateLong>(random.nextInt(2000)),
                new StateLong(random.nextInt(2000)));
    randomInsertionsAndRemove(nextEntry, false);
    randomInsertionsAndRemove(nextEntry, true);
  }

  /**
   * AvlTree must satisfy reasonable {@link Map} properties: Map must be empty after removing
   * everything.
   */
  private <@ImmutableTypeParameter K extends Comparable<K>> void randomInsertionsAndRemove(
      Supplier<Map.Entry<K, StateLong>> nextEntry, boolean randomRemovalOrder) {

    // Create AvlTree.
    final Map<K, StateLong> entries = new LinkedHashMap<>();
    AvlTree<K, StateLong> tree = createInitialTree(entries, nextEntry);

    // Check AvlTree matches expected entries
    for (Map.Entry<K, StateLong> entry : entries.entrySet()) {
      Assertions.assertThat(tree.getValue(entry.getKey())).isEqualTo(entry.getValue());
    }

    // Print tree
    System.out.println("Tree");
    System.out.println(avlTreeToString(0, tree));

    // Remove keys, possibly in random order
    final List<K> keys = new ArrayList<>(entries.keySet());
    if (randomRemovalOrder) {
      Collections.shuffle(keys, random);
    }
    for (final K key : keys) {
      tree = tree.remove(key);
      assertAvlTreeInvariants(tree);
    }

    // Check empty
    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  private static <@ImmutableTypeParameter K extends Comparable<K>>
      AvlTree<K, StateLong> createInitialTree(
          Map<K, StateLong> mapMirror, Supplier<Map.Entry<K, StateLong>> nextEntry) {
    AvlTree<K, StateLong> tree = AvlTree.create();

    for (int i = 0; i < 1000; i++) {
      final Map.Entry<K, StateLong> entry = nextEntry.get();
      boolean alreadyPresent = mapMirror.containsKey(entry.getKey());
      Assertions.assertThat(tree.containsKey(entry.getKey())).isEqualTo(alreadyPresent);
      if (!alreadyPresent) {
        Assertions.assertThat(tree.getValue(entry.getKey())).isNull();
      }
      mapMirror.put(entry.getKey(), entry.getValue());
      tree = tree.set(entry.getKey(), entry.getValue());
      assertAvlTreeInvariants(tree);
      Assertions.assertThat(tree.size()).isEqualTo(mapMirror.size());
    }

    return tree;
  }

  /** null key is not allowed in {@link AvlTree}. */
  @Test
  public void nullKeysAreNotAllowed() {
    final AvlTree<Integer, StateLong> tree = AvlTree.create();

    // Empty
    assertNullKeysAreNotAllowed(tree);

    // Leaf
    assertNullKeysAreNotAllowed(tree.set(1, new StateLong(1)));

    // Composite
    assertNullKeysAreNotAllowed(tree.set(1, new StateLong(1)).set(2, new StateLong(1)));
  }

  private static void assertNullKeysAreNotAllowed(AvlTree<Integer, StateLong> tree) {
    Assertions.assertThatCode(() -> tree.set(null, new StateLong(1)))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("null key is not allowed in AvlTree");

    Assertions.assertThatCode(() -> tree.getValue(null))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("null key is not allowed in AvlTree");

    Assertions.assertThatCode(() -> tree.remove(null))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("null key is not allowed in AvlTree");

    Assertions.assertThatCode(() -> tree.containsKey(null))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("null key is not allowed in AvlTree");

    Assertions.assertThatCode(() -> tree.containsKey(null))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("null key is not allowed in AvlTree");

    Assertions.assertThatCode(() -> tree.visitPath(null, null, null))
        .isInstanceOf(NullPointerException.class)
        .hasMessage("null key is not allowed in AvlTree");
  }

  /** Null values are allowed in {@link AvlTree}. */
  @Test
  public void nullValues() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(1, null);
    tree = tree.set(2, null);

    Assertions.assertThat(tree.size()).isEqualTo(2);
    Assertions.assertThat(tree.getValue(1)).isNull();
    Assertions.assertThat(tree.getValue(2)).isNull();
    assertAvlTreeInvariants(tree);

    Assertions.assertThat(tree.values()).containsExactly(null, null);

    writeReadTree(tree, populate);
  }

  /** AvlTree will automatically balance itself. */
  @Test
  public void piBoundaryInBalance() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(121, new StateLong(1));
    tree = tree.set(222, new StateLong(1));
    tree = tree.set(929, new StateLong(1));
    assertAvlTreeInvariants(tree);
    tree = tree.set(1000, new StateLong(1));
    tree = tree.set(1001, new StateLong(1));
    assertAvlTreeInvariants(tree);
    tree = tree.remove(929);
    assertAvlTreeInvariants(tree);
  }

  /** {@link AvlTree#remove} is idempotent. */
  @Test
  public void removeFromEmpty() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.remove(0);
    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  /** Getting an unknown value produces null. */
  @Test
  public void getFromEmpty() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Assertions.assertThat(tree.getValue(0)).isNull();
  }

  /** {@link AvlTree#remove} is idempotent. */
  @Test
  public void removeNonExisting() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(1, new StateLong(2)).remove(2);

    Assertions.assertThat(tree.size()).isEqualTo(1);
  }

  @Test
  public void removeRandomOrder() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    List<Integer> values = IntStream.range(0, 100).boxed().collect(Collectors.toList());
    for (int i = 0; i < values.size(); i++) {
      Assertions.assertThat(tree.size()).isEqualTo(i);
      Integer value = values.get(i);
      tree = tree.set(value, new StateLong(value));
      assertAvlTreeInvariants(tree);
    }

    ArrayList<Integer> shuffled = new ArrayList<>(values);
    Collections.shuffle(shuffled, random);
    for (int i = shuffled.size() - 1; i >= 0; i--) {
      Integer key = shuffled.get(i);
      tree = tree.remove(key);
      assertAvlTreeInvariants(tree);
      Assertions.assertThat(tree.size()).isEqualTo(i);
      // Safe to remove twice
      tree = tree.remove(key);
    }
    Assertions.assertThat(tree.size()).isEqualTo(0);
  }

  @Test
  public void keySet() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Assertions.assertThat(tree.keySet()).isEmpty();
    tree = tree.set(11, new StateLong(22));
    Assertions.assertThat(tree.keySet()).containsExactlyInAnyOrder(11);
    tree = tree.set(99, new StateLong(22));
    Assertions.assertThat(tree.keySet()).containsExactlyInAnyOrder(11, 99);
  }

  @Test
  public void values() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Assertions.assertThat(tree.values()).isEmpty();
    tree = tree.set(11, new StateLong(22));
    Assertions.assertThat(tree.values()).hasSize(1);
    Assertions.assertThat(tree.values().get(0).value()).isEqualTo(22);
    tree = tree.set(99, new StateLong(22));
    Assertions.assertThat(tree.values()).hasSize(2);
    Assertions.assertThat(tree.values().get(0).value()).isEqualTo(22);
    Assertions.assertThat(tree.values().get(1).value()).isEqualTo(22);
  }

  @Test
  public void fromMapShouldBeStable() {
    LinkedHashMap<Integer, StateLong> first = new LinkedHashMap<>();
    first.put(1, new StateLong(1));
    first.put(2, new StateLong(2));
    first.put(3, new StateLong(3));

    LinkedHashMap<Integer, StateLong> second = new LinkedHashMap<>();
    second.put(3, new StateLong(3));
    second.put(2, new StateLong(2));
    second.put(1, new StateLong(1));

    AvlTree<Integer, StateLong> firstAvl = AvlTree.create(first);
    AvlTree<Integer, StateLong> secondAvl = AvlTree.create(second);

    WithAvlTree firstSerializable = new WithAvlTree(firstAvl);
    Assertions.assertThat(firstSerializable.tree()).isEqualTo(firstAvl);
    Hash firstHash = populate.createHash(firstSerializable);
    Hash secondHash = populate.createHash(new WithAvlTree(secondAvl));
    Assertions.assertThat(firstHash).isEqualTo(secondHash);
  }

  @Test
  public void getNextZero() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(11, new StateLong(22));
    Assertions.assertThat(tree.getNextN(11, 0)).hasSize(0);
    Assertions.assertThat(tree.getNextN(11, -1)).hasSize(0);
    Assertions.assertThat(tree.getNextN(null, 0)).hasSize(0);
    tree = tree.set(99, new StateLong(22));
    Assertions.assertThat(tree.getNextN(11, 0)).hasSize(0);
    Assertions.assertThat(tree.getNextN(22, 0)).hasSize(0);
    Assertions.assertThat(tree.getNextN(11, -1)).hasSize(0);
    Assertions.assertThat(tree.getNextN(22, -1)).hasSize(0);
    Assertions.assertThat(tree.getNextN(null, -1)).hasSize(0);
  }

  @Test
  public void getNextKeyNotPresent() {
    AvlTree<Integer, Long> tree = AvlTree.create();
    tree = tree.set(11, 22L);
    Assertions.assertThat(tree.getNextN(5, 2)).containsExactly(Map.entry(11, 22L));
    Assertions.assertThat(tree.getNextN(15, 2)).containsExactly();
    tree = tree.set(22, 44L);
    Assertions.assertThat(tree.getNextN(5, 2))
        .containsExactly(Map.entry(11, 22L), Map.entry(22, 44L));
    Assertions.assertThat(tree.getNextN(15, 2)).containsExactly(Map.entry(22, 44L));
    tree = tree.set(33, 66L);
    Assertions.assertThat(tree.getNextN(5, 2))
        .containsExactly(Map.entry(11, 22L), Map.entry(22, 44L));
    Assertions.assertThat(tree.getNextN(15, 2))
        .containsExactly(Map.entry(22, 44L), Map.entry(33, 66L));
  }

  @Test
  public void getNextExcludesKey() {
    AvlTree<Integer, Long> tree = AvlTree.create();
    tree = tree.set(11, 22L);
    Assertions.assertThat(tree.getNextN(11, 2)).containsExactly();
    tree = tree.set(22, 44L);
    tree = tree.set(33, 66L);
    Assertions.assertThat(tree.getNextN(11, 2))
        .containsExactly(Map.entry(22, 44L), Map.entry(33, 66L));
    Assertions.assertThat(tree.getNextN(22, 2)).containsExactly(Map.entry(33, 66L));
  }

  @Test
  public void getFirstN() {
    AvlTree<Integer, Long> tree = AvlTree.create();
    tree = tree.set(11, 22L);
    tree = tree.set(22, 44L);
    tree = tree.set(33, 66L);
    Assertions.assertThat(tree.getNextN(null, 2))
        .containsExactly(Map.entry(11, 22L), Map.entry(22, 44L));
  }

  @Test
  public void getNextN() {
    Map<Integer, Long> map =
        IntStream.range(0, 100).boxed().collect(Collectors.toMap(i -> i, i -> (long) i));
    AvlTree<Integer, Long> tree = AvlTree.create(map);
    for (int i = 0; i < 100; i++) {
      List<Map.Entry<Integer, Long>> values = tree.getNextN(i, 10);
      List<Map.Entry<Integer, Long>> expected =
          IntStream.range(i + 1, min(i + 11, 100))
              .boxed()
              .map(j -> Map.entry(j, (long) j))
              .toList();
      Assertions.assertThat(values).isEqualTo(expected);
    }
  }

  /** Only get the specified max number of elements. */
  @Test
  public void modifiedMaxNumToGet() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(6, new StateLong(6));
    tree = tree.set(2, new StateLong(2));
    tree = tree.set(7, new StateLong(7));
    tree = tree.set(4, new StateLong(4));
    final AvlTree<Integer, StateLong> oldTree = writeReadTree(tree, populate);
    tree = tree.set(5, new StateLong(5));
    tree = tree.set(3, new StateLong(3));
    tree = tree.set(8, new StateLong(8));
    tree = tree.set(9, new StateLong(9));

    tree = writeReadTree(tree, populate);

    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, null, 1))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, null, 1))
        .hasSize(1)
        .contains(3);

    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, null, 3))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, null, 3))
        .hasSize(3)
        .contains(3, 5, 8);

    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, null, 10))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, null, 10))
        .hasSize(4)
        .contains(3, 5, 8, 9);
  }

  /** When giving a starting key, only get modified keys greater than the starting key. */
  @Test
  public void modifiedFromKey() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(2, new StateLong(2));
    tree = tree.set(4, new StateLong(4));
    tree = tree.set(7, new StateLong(7));
    tree = tree.set(8, new StateLong(8));
    final AvlTree<Integer, StateLong> oldTree = writeReadTree(tree, populate);
    tree = tree.set(3, new StateLong(3));
    tree = tree.set(5, new StateLong(5));
    tree = tree.set(6, new StateLong(6));
    tree = tree.set(9, new StateLong(9));

    tree = writeReadTree(tree, populate);

    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, 2, 10))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, 2, 10))
        .hasSize(4)
        .contains(3, 5, 6, 9);
    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, 3, 10))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, 3, 10))
        .hasSize(3)
        .contains(5, 6, 9);
    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, 6, 10))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, 6, 10))
        .hasSize(1)
        .contains(9);
    Assertions.assertThat(AvlTree.modifiedKeys(tree, oldTree, 9, 10))
        .isEqualTo(AvlTree.modifiedKeys(oldTree, tree, 9, 10))
        .hasSize(0)
        .contains();
  }

  /** When adding or removing a single element to a tree, then modified keys returns that key. */
  @Test
  public void modifiedKeysReading() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    for (int i = 0; i < 1000; i++) {
      AvlTree<Integer, StateLong> newTree = writeReadTree(tree.set(i, new StateLong(i)), populate);
      List<Integer> modified = AvlTree.modifiedKeys(tree, newTree, null, 10);
      Assertions.assertThat(modified).hasSize(1).contains(i);
      tree = newTree;
    }
    for (int i = 0; i < 1000; i++) {
      AvlTree<Integer, StateLong> newTree = writeReadTree(tree.remove(i), populate);
      List<Integer> modified = AvlTree.modifiedKeys(tree, newTree, null, 10);
      Assertions.assertThat(modified).hasSize(1).contains(i);
      tree = newTree;
    }
  }

  /**
   * For a random number of inserts of random keys, all inserted keys are returned in sorted order.
   */
  @Test
  public void modifiedKeysRandomReading() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Random random = new Random(1234);
    int freshValue = 0;
    for (int i = 0; i < 100; i++) {
      // Make a random number of changes to the tree
      int randomNumberOfInserts = random.nextInt(50);
      AvlTree<Integer, StateLong> otherTree = tree;
      SortedSet<Integer> expectedModifiedKey = new TreeSet<>();
      for (int j = 0; j < randomNumberOfInserts; j++) {
        int randomKey = random.nextInt(10000);
        expectedModifiedKey.add(randomKey);
        otherTree = otherTree.set(randomKey, new StateLong(freshValue++));
      }

      AvlTree<Integer, StateLong> newTree = writeReadTree(otherTree, populate);
      List<Integer> modified = AvlTree.modifiedKeys(tree, newTree, null, 50);
      Assertions.assertThat(modified).isEqualTo(expectedModifiedKey.stream().toList());
      tree = newTree;
    }
  }

  /**
   * For a random number of inserts of random keys, when giving a key to start from only inserted
   * keys greater than the starting key are returned in sorted order.
   */
  @Test
  public void modifiedKeysRandomReadingFromKey() {
    int fromKey = 500;
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    Random random = new Random(1234);
    int freshValue = 0;
    for (int i = 0; i < 100; i++) {
      // Make a random number of changes to the tree
      int randomNumberOfInserts = random.nextInt(50);
      SortedSet<Integer> expectedModifiedKey = new TreeSet<>();
      AvlTree<Integer, StateLong> otherTree = tree;
      for (int j = 0; j < randomNumberOfInserts; j++) {
        int randomKey = random.nextInt(10000);
        if (randomKey > fromKey) {
          expectedModifiedKey.add(randomKey);
        }
        otherTree = otherTree.set(randomKey, new StateLong(freshValue++));
      }

      AvlTree<Integer, StateLong> newTree = writeReadTree(otherTree, populate);
      List<Integer> modified = AvlTree.modifiedKeys(tree, newTree, fromKey, 50);
      Assertions.assertThat(modified).isEqualTo(expectedModifiedKey.stream().toList());
      tree = newTree;
    }
  }

  /** When writing a freshly read leaf, it should have the same size as the original write. */
  @Test
  void writeReadLeafSameSize() {
    AvlTree<Integer, StateLong> tree = AvlTree.create();
    tree = tree.set(1, new StateLong(1));
    var expectedTreeWrite = populate.write(new WithAvlTree(tree));
    AvlTree<Integer, StateLong> newTree =
        populate.read(expectedTreeWrite.hash(), WithAvlTree.class).tree();

    var writeResult = populate.write(new WithAvlTree(newTree));
    Assertions.assertThat(writeResult.totalByteCount())
        .isEqualTo(expectedTreeWrite.totalByteCount());
  }

  /** The written size of a composite avl map should not change if keys are only updated. */
  @Test
  void writeReadCompositeSameSize() {
    Map<Integer, StateLong> map =
        IntStream.range(0, 100).boxed().collect(Collectors.toMap(i -> i, i -> new StateLong(i)));
    AvlTree<Integer, StateLong> tree = AvlTree.create(map);
    var expectedTreeWrite = populate.write(new WithAvlTree(tree));
    AvlTree<Integer, StateLong> newTree =
        populate.read(expectedTreeWrite.hash(), WithAvlTree.class).tree();
    newTree = newTree.set(1, new StateLong(-1L));
    newTree = newTree.set(3, new StateLong(-3L));

    var writeResult = populate.write(new WithAvlTree(newTree));
    Assertions.assertThat(writeResult.totalByteCount())
        .isEqualTo(expectedTreeWrite.totalByteCount());
  }

  @Nested
  final class TestSkippingElements {

    @DisplayName("Can skip elements from the beginning")
    @Test
    void skipFromStart() {
      Map<Integer, Long> map =
          IntStream.range(0, 100).boxed().collect(Collectors.toMap(i -> i, i -> (long) i));
      AvlTree<Integer, Long> tree = AvlTree.create(map);

      assertContainsRange(tree.getNextN(null, 20, 0), 0, 20);
      assertContainsRange(tree.getNextN(null, 20, 20), 20, 40);
      assertContainsRange(tree.getNextN(null, 20, 40), 40, 60);
      assertContainsRange(tree.getNextN(null, 20, 60), 60, 80);
      assertContainsRange(tree.getNextN(null, 20, 80), 80, 100);
      assertContainsRange(tree.getNextN(null, 20, 90), 90, 100);
    }

    @DisplayName("Can skip elements from a given key")
    @Test
    void skipFromKey() {
      Map<Integer, Long> map =
          IntStream.range(0, 100).boxed().collect(Collectors.toMap(i -> i, i -> (long) i));
      AvlTree<Integer, Long> tree = AvlTree.create(map);

      assertContainsRange(tree.getNextN(29, 20, 0), 30, 50);
      assertContainsRange(tree.getNextN(29, 20, 20), 50, 70);
      assertContainsRange(tree.getNextN(29, 20, 40), 70, 90);
      assertContainsRange(tree.getNextN(29, 20, 60), 90, 100);
    }

    @DisplayName("Does not read skipped elements from storage")
    @Test
    void skippedAreNotRead() {
      Map<Integer, StateLong> map =
          IntStream.range(0, 100).boxed().collect(Collectors.toMap(i -> i, StateLong::new));
      AvlTree<Integer, StateLong> tree = AvlTree.create(map);
      tree = writeReadTree(tree, populate);
      int objectsReadBefore = storage.getObjectsRead();
      List<Map.Entry<Integer, StateLong>> nextN = tree.getNextN(null, 20, 60);
      Assertions.assertThat(nextN.size()).isEqualTo(20);
      int objectsReadAfter = storage.getObjectsRead();
      Assertions.assertThat(objectsReadAfter - objectsReadBefore).isEqualTo(47);
    }

    private void assertContainsRange(List<Map.Entry<Integer, Long>> nextN, int beginning, int end) {
      List<Integer> range = IntStream.range(beginning, end).boxed().toList();
      Assertions.assertThat(nextN.size()).isEqualTo(range.size());
      for (int i = 0; i < nextN.size(); i++) {
        Assertions.assertThat(nextN.get(i).getKey()).isEqualTo(range.get(i));
        Assertions.assertThat(nextN.get(i).getValue()).isEqualTo((long) range.get(i));
      }
    }
  }

  /**
   * Write and read the tree to/from storage to populate the hashes.
   *
   * @param tree tree to write
   * @param populate state serializer
   * @return the read tree
   */
  private static AvlTree<Integer, StateLong> writeReadTree(
      AvlTree<Integer, StateLong> tree, StateSerializer populate) {
    var result = populate.write(new WithAvlTree(tree));
    return populate.read(result.hash(), WithAvlTree.class).tree();
  }

  private static <K extends Comparable<K>> String avlTreeToString(
      int indent, AvlTree<K, StateLong> node) {
    if (node == null) {
      return "Empty";
    }
    StringBuilder r = new StringBuilder();
    r.append("-".repeat(indent));
    r.append(node.getKey());
    r.append("\n");
    if (node instanceof AvlComposite) {
      r.append(avlTreeToString(indent + 1, ((AvlComposite<K, StateLong>) node).getLeft()));
      r.append(avlTreeToString(indent + 1, ((AvlComposite<K, StateLong>) node).getRight()));
    }
    return r.toString();
  }

  /**
   * Checks and asserts that given {@link AvlTree} satisifes the required invariants.
   *
   * @see #satisfiesAvlTreeInvariants
   */
  private static <K extends Comparable<K>> void assertAvlTreeInvariants(
      AvlTree<K, StateLong> node) {
    Assertions.assertThat(satisfiesAvlTreeInvariants(node)).as("AvlTree invariants").isTrue();

    if (node instanceof EmptyTree) {
      Assertions.assertThat(node.size()).as("AvlTree size").isEqualTo(0);
    } else {
      Assertions.assertThat(node.size()).as("AvlTree size").isGreaterThan(0);
    }
  }

  /**
   * Checks whether the given {@link AvlTree} satisifes the expected invariants.
   *
   * <p>Checked invariants:
   *
   * <ul>
   *   <li>Tree must be balanced and correctly ordered.
   *   <li>{@link EmptyTree} can only occur in an {@link EmptyTree}, and must not occur as a
   *       subtree.
   * </ul>
   */
  private static <K extends Comparable<K>> boolean satisfiesAvlTreeInvariants(
      AvlTree<K, StateLong> node) {
    if (node instanceof AvlComposite) {
      final AvlTree<K, StateLong> left = ((AvlComposite<K, StateLong>) node).getLeft();
      final AvlTree<K, StateLong> right = ((AvlComposite<K, StateLong>) node).getRight();
      final K leftMax = left.maxKeyInTree();
      final K rightMin = minKeyInTree(right);
      boolean leftOk = leftMax.compareTo(node.getKey()) == 0;
      boolean rightOk = rightMin.compareTo(node.getKey()) > 0;
      boolean keyOk = leftOk && rightOk;
      if (!keyOk) {
        System.out.println("Error at " + node.getKey());
        System.out.println("------------------");
      }
      return keyOk
          && satisfiesAvlTreeInvariants(left)
          && satisfiesAvlTreeInvariants(right)
          && satisfiesNestedAvlTreeInvariants(left)
          && satisfiesNestedAvlTreeInvariants(right)
          && Math.abs(node.balance()) <= 1;
    }
    return true;
  }

  private static <K extends Comparable<K>> boolean satisfiesNestedAvlTreeInvariants(
      AvlTree<K, StateLong> node) {
    return !(node instanceof EmptyTree);
  }

  /**
   * Determines the key with the least value in the given {@link Comparable}-keyed {@link AvlTree}.
   */
  private static <K extends Comparable<K>> K minKeyInTree(AvlTree<K, StateLong> node) {
    if (node instanceof AvlComposite) {
      AvlTree<K, StateLong> left = ((AvlComposite<K, StateLong>) node).getLeft();
      AvlTree<K, StateLong> right = ((AvlComposite<K, StateLong>) node).getRight();
      return min(minKeyInTree(left), minKeyInTree(right));
    } else {
      return node.getKey();
    }
  }

  private static <T extends Comparable<T>> T min(T left, T right) {
    return left.compareTo(right) < 0 ? left : right;
  }

  @Override
  public String toString() {
    return "AvlTreeTest{" + "random=" + random + '}';
  }

  /** For testing nested {@link AvlTree}. */
  @Immutable
  record WithAvlTree(AvlTree<Integer, StateLong> tree) implements StateSerializable {}

  /** For testing record keys in {@link AvlTree}. */
  @Immutable
  record MyId(int rawId) implements StateSerializable, Comparable<MyId> {
    @Override
    public int compareTo(MyId o) {
      return Integer.compare(this.rawId(), o.rawId());
    }
  }

  /** For testing generic record keys in {@link AvlTree}. */
  @Immutable
  record MyGenericId<V extends StateSerializable>(int rawId)
      implements StateSerializable, Comparable<MyGenericId<V>> {
    @Override
    public int compareTo(MyGenericId<V> o) {
      return Integer.compare(this.rawId(), o.rawId());
    }
  }
}
