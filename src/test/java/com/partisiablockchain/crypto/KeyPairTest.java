package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.crypto.Curve.CURVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import com.partisiablockchain.BlockchainAddress;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.signers.DSAKCalculator;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.crypto.signers.RandomDSAKCalculator;
import org.bouncycastle.crypto.util.DigestFactory;
import org.junit.jupiter.api.Test;

/** Test. */
public final class KeyPairTest {

  @Test
  public void canSignAndRecoverPublicKeyFromPrivateKeyOne() {
    signAndRecoverPublicKeyFromPrivateKey(BigInteger.ONE);
  }

  @Test
  public void canSignAndRecoverPublicKeyFromPrivateKeyTwo() {
    signAndRecoverPublicKeyFromPrivateKey(BigInteger.TWO);
  }

  @Test
  public void canSignAndRecoverPublicKeyFromManyPrivateKeys() {
    for (int i = 0; i < 100; i++) {
      signAndRecoverPublicKeyFromPrivateKey(new BigInteger("11234557" + i));
    }
  }

  private void signAndRecoverPublicKeyFromPrivateKey(BigInteger privateKey) {
    KeyPair keyPair = new KeyPair(privateKey);
    byte[] message = new byte[32];
    Hash messageHash = Hash.create(out -> out.write(message));
    Signature signature = keyPair.sign(messageHash);

    assertThat(signature).isNotNull();

    BlockchainAddress sender = signature.recoverSender(messageHash);
    assertThat(sender).isEqualTo(keyPair.getPublic().createAddress());
  }

  @Test
  public void nonRecoverableSignature() {
    KeyPair keyPair = new KeyPair(BigInteger.TEN);
    Hash messageHash = Hash.create(FunctionUtility.noOpConsumer());
    Signature signature = keyPair.sign(messageHash);

    Signature modifiedSignature =
        new Signature(signature.getRecoveryId() + 1 % 4, signature.getR(), signature.getS());
    assertThat(modifiedSignature.recoverSender(messageHash)).isNull();
  }

  @Test
  public void publicPointFromPrivateWithLargeBitSize() {
    KeyPair keyPair = new KeyPair(CURVE.getN().multiply(BigInteger.TWO).add(BigInteger.TEN));
    assertThat(keyPair.getPrivateKey()).isEqualTo(BigInteger.TEN);
  }

  @Test
  public void publicPointFromPrivateWithExactBitSize() {
    BigInteger key = CURVE.getN().add(BigInteger.TEN);
    KeyPair keyPair = new KeyPair(key);
    assertThat(keyPair.getPrivateKey()).isEqualTo(key);
  }

  @Test
  public void shouldNotNormalizeHalfCurve() {
    boolean requiresNormalization = KeyPair.requiresNormalization(Curve.HALF_CURVE_ORDER);
    assertThat(requiresNormalization).isEqualTo(false);
  }

  @Test
  public void shouldNormalizeHalfCurvePlusOne() {
    boolean requiresNormalization =
        KeyPair.requiresNormalization(Curve.HALF_CURVE_ORDER.add(BigInteger.ONE));
    assertThat(requiresNormalization).isEqualTo(true);
  }

  @Test
  public void randomNonceProducerDecoratedIsNotDeterministic() {
    DSAKCalculator nonceProducer =
        new KeyPair.NonceProducerMemoryDecorator(new RandomDSAKCalculator());

    assertThat(nonceProducer.isDeterministic()).isFalse();
  }

  @Test
  public void initialisingDeterministicNoneProducerWithRandomnessProducesError() {
    KeyPair.NonceProducerMemoryDecorator nonceProducer =
        new KeyPair.NonceProducerMemoryDecorator(
            new HMacDSAKCalculator(DigestFactory.createSHA256()));
    try {
      nonceProducer.init(BigInteger.ONE, new SecureRandom());
      fail("Expected exception but did not encounter any");
    } catch (IllegalStateException e) {
      assertThat(e.getMessage()).isEqualTo("Operation not supported");
    }
  }

  @Test
  public void decoratedRandomNonceProducerCanExtractNonce() {
    KeyPair.NonceProducerMemoryDecorator nonceProducer =
        new KeyPair.NonceProducerMemoryDecorator(new RandomDSAKCalculator());
    nonceProducer.init(BigInteger.TEN, new SecureRandom());
    BigInteger k = nonceProducer.nextK();

    assertThat(k).isEqualTo(nonceProducer.getLastK());
  }
}
