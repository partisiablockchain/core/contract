package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class CurveTest {

  @Test
  public void listSerializer() {
    List<BlockchainPublicKey> collect =
        Stream.generate(() -> new KeyPair().getPublic()).limit(5).collect(Collectors.toList());

    byte[] serialize =
        SafeDataOutputStream.serialize(
            s -> BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(s, collect));
    List<BlockchainPublicKey> deserialize =
        SafeDataInputStream.deserialize(
            BlockchainPublicKey.LIST_SERIALIZER::readDynamic, serialize);

    Assertions.assertThat(deserialize).isEqualTo(collect);
  }

  @Test
  public void singlePointSerialization() {
    BlockchainPublicKey key = new KeyPair().getPublic();

    byte[] serializedKey = SafeDataOutputStream.serialize(key::write);

    BlockchainPublicKey restoredKey =
        SafeDataInputStream.deserialize(BlockchainPublicKey::read, serializedKey);

    Assertions.assertThat(restoredKey).isEqualTo(key);
  }
}
