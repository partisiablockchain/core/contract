package com.partisiablockchain.crypto.bls;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PairingTest {
  private static final G2 POINT_AT_INFINITY =
      new G2(Fq2.createZero(), Fq2.createOne(), Fq2.createZero());

  private static final Random rnd = new Random();
  private static final int bits = 380;

  /** Repeatedly tests for both miller loops return the same 10 times on random values. */
  @RepeatedTest(10)
  public void millerLoopPrecomputedIsSameAsMillerLoop() {
    BigInteger scalar = new BigInteger(bits, rnd);
    G1 g1Scaled = G1.G.scalePoint(NafEncoded.create(scalar));
    G2 g2Scaled = G2.G.scalePoint(NafEncoded.create(scalar));
    Fq2[][] g2Computed = Pairing.prepareG2PointForPairing(g2Scaled);
    Assertions.assertThat(Pairing.millerLoop(g1Scaled, g2Scaled))
        .isEqualTo(Pairing.millerLoop(g1Scaled, g2Computed));
  }

  @Test
  public void identity() {
    Gt ident1 = Pairing.computePairing(G1.POINT_AT_INFINITY, G2.G);
    Assertions.assertThat(ident1).isEqualTo(Gt.createOne());
    Gt ident2 = Pairing.computePairing(G1.G, POINT_AT_INFINITY);
    Assertions.assertThat(ident2).isEqualTo(Gt.createOne());
  }

  /** Repeatedly tests for non-generate 10 times on random values. */
  @RepeatedTest(10)
  public void nonGenerate() {
    // e(G1, G2) = 1 only if G1 or G2 is POINT AT INFINITY (identity)
    BigInteger scalar = new BigInteger(bits, rnd);
    G1 g1Scaled = G1.G.scalePoint(NafEncoded.create(scalar));
    G2 g2Scaled = G2.G.scalePoint(NafEncoded.create(scalar));
    Assertions.assertThat(Pairing.computePairing(g1Scaled, G2.G)).isNotEqualTo(Gt.createOne());
    Assertions.assertThat(Pairing.computePairing(G1.G, g2Scaled)).isNotEqualTo(Gt.createOne());
  }

  /** Repeatedly tests for bilinearity 10 times on random values. */
  @RepeatedTest(10)
  public void bilinear() {
    // Test that e(x·G1, G2) = e(G1, x·G2) = e(G1, G2)^x
    NafEncoded x = NafEncoded.create(new BigInteger(bits, rnd));
    G1 scale1 = G1.G.scalePoint(x);
    Gt result1 = Pairing.computePairing(scale1, G2.G);
    Assertions.assertThat(result1).isNotEqualTo(Gt.createOne());
    G2 scale2 = G2.G.scalePoint(x);
    Gt result2 = Pairing.computePairing(G1.G, scale2);
    Assertions.assertThat(result2).isNotEqualTo(Gt.createOne());
    Assertions.assertThat(result1).isEqualTo(result2);
    Assertions.assertThat(Pairing.computePairing(G1.G, G2.G).exp(x)).isEqualTo(result2);
  }

  @Test
  public void gtIsNotOne() {
    Gt g = Pairing.computePairing(G1.G, G2.G);
    Gt pow = g.exp(NafEncoded.create(BigInteger.TWO));
    Assertions.assertThat(pow).isNotEqualTo(Gt.createOne());
  }

  @Test
  public void gtGenerator() {
    Gt g = Pairing.computePairing(G1.G, G2.G);
    Gt pow = g.exp(Pairing.order);
    Assertions.assertThat(pow).isEqualTo(Gt.createOne());
  }
}
