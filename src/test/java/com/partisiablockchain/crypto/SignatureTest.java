package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.math.BigInteger;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.bouncycastle.math.ec.custom.sec.SecP256K1Curve;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SignatureTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(Signature.class).verify();
  }

  @Test
  public void illegalRecoveryId() {
    Assertions.assertThatThrownBy(() -> new Signature(-1, BigInteger.ZERO, BigInteger.ZERO))
        .isInstanceOf(IllegalArgumentException.class);
    Assertions.assertThatThrownBy(() -> new Signature(4, BigInteger.ZERO, BigInteger.ZERO))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void checkValuesBoundary() {
    new Signature(0, BigInteger.ZERO, BigInteger.ZERO);
  }

  @Test
  public void illegalR() {
    Assertions.assertThatThrownBy(() -> new Signature(0, BigInteger.valueOf(-1), BigInteger.ONE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("r must be positive");
  }

  @Test
  public void illegalS() {
    Assertions.assertThatThrownBy(() -> new Signature(0, BigInteger.ONE, BigInteger.valueOf(-1)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("s must be positive");
  }

  @Test
  public void toAndFromString() {
    BigInteger valueR = BigInteger.valueOf(0b11111111);
    Signature signature = new Signature(0, valueR, BigInteger.TWO);
    assertThat(signature.getR()).isEqualTo(valueR);
    assertThat(signature.getS()).isEqualTo(BigInteger.TWO);
    assertThat(signature.getRecoveryId()).isEqualTo(0);

    String asString = signature.writeAsString();
    assertThat(asString).hasSize(65 * 2);

    assertThat(Signature.fromString(asString)).isEqualTo(signature);
  }

  @Test
  public void toAndFromBytes() {
    BigInteger valueR = BigInteger.valueOf(0b11111111);
    Signature signature = new Signature(3, valueR, BigInteger.TWO);
    assertThat(signature.getR()).isEqualTo(valueR);
    assertThat(signature.getS()).isEqualTo(BigInteger.TWO);
    assertThat(signature.getRecoveryId()).isEqualTo(3);

    byte[] asBytes = SafeDataOutputStream.serialize(signature::write);
    assertThat(asBytes).hasSize(65);

    assertThat(Signature.read(SafeDataInputStream.createFromBytes(asBytes))).isEqualTo(signature);
  }

  @Test
  public void stableHash() {
    Signature signature = new Signature(0, BigInteger.valueOf(777L), BigInteger.TWO);
    Hash hash = Hash.create(signature);
    assertThat(hash.toString())
        .isEqualTo("a13685e30b073faa4a7ae6cc0de859957b0869397cd5af07f69f2ee9017d1171");
  }

  @Test
  public void nullMessageHash() {
    Assertions.assertThatThrownBy(
            () -> new Signature(0, BigInteger.ONE, BigInteger.ONE).recoverPublicKey(null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("messageHash must not be null");
  }

  @Test
  public void largerThanPrimeX() {
    SecP256K1Curve curve = (SecP256K1Curve) Curve.CURVE.getCurve();
    BigInteger prime = curve.getQ();
    Signature signature = new Signature(0, prime, BigInteger.ZERO);
    BlockchainPublicKey publicKey =
        signature.recoverPublicKey(Hash.create(FunctionUtility.noOpConsumer()));
    assertThat(publicKey).isNull();
  }

  @Test
  public void makeSureBinaryMutationsInRecoverFails() {
    Signature signature =
        Signature.fromString(
            "01c233d13c0b282027500fb5c0f454adcc45f4482d2de0960213eca8940511ee60048f36986a6016"
                + "9cb72ce720226d26b2a67809d02c77f6e84055f56ed553ab5b");
    Hash messageHash = Hash.create(FunctionUtility.noOpConsumer());
    BlockchainPublicKey publicKey = signature.recoverPublicKey(messageHash);
    assertThat(publicKey).isEqualTo(new KeyPair(BigInteger.TEN).getPublic());
  }
}
